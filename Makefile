#CXX=clang
CPPFLAGS= -g -std=c++17 -fdiagnostics-color=always
#CPPFLAGS+= -fsanitize=address 
HEAD=28



algo_base_test: clean
	@echo -en "\n\n\n\n          COMPILE\n\n"
	@make $@.o 2>&1 | head -n $(HEAD)
	@echo -en "\033[0m"
	@g++ $@.o -o $@.out


board_test: clean
	@echo -en "\n\n\n\n          COMPILE\n\n"
	@make $@.o 2>&1 | head -n $(HEAD)
	@echo -en "\033[0m"
	@g++ $@.o -o $@.out
	@./$@.out
	
.PHONY: clean

clean: 
	@rm -f *.o *.out *.tmp 
