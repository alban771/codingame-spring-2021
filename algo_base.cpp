#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <cassert>
#include <type_traits>

#include "board.cpp"

#ifndef _ALGOBASE_
#define _ALGOBASE_
#define DEFENSIVE


using namespace std;


template<typename T>
struct cell 
{
    using board_type = board<T>;
    using board_ref = board_type *;
    using cell_id = board_types::cell_id;
    using orientation = board_types::orientation;
    using difference_type = typename board_iterator<T>::difference_type;

public:
    constexpr cell(const cell_id i = -1): _board(nullptr), id(i) {}

    const auto begin() const
    {
      return _board->cells.begin();
    }
    const auto end() const
    {
      return _board->cells.end();
    }

    vector<T> range( const orientation ori, const difference_type dist = 3 ) const
    {
      board_iterator<T> it( *_board, id, ori ), ite = it.end();
      vector<T> s( min( (ite-it)-1, dist ) ); // exclude 0
      generate( s.begin(), s.end(), [&it]()  
      {
        return *(++it);
      });
      return s;
    }

    vector<T> range( const vector<orientation>& ori, const difference_type dist = 3 ) const
    {
      vector<T> s(dist * ori.size());
      for_each( ori.begin(), ori.end(), 
      [&,i=0] (const int o) mutable
      {
          const vector<T> r = range(o, dist);
          copy(r.begin(), r.end(), s.begin() + dist*i++ );
      });
      return s;
    }

    vector<T> arc( const int radius = 2 ) const
    {
      vector<T> s;
      arc_iterator<T> it( *_board, id, radius ), ite = it.end();
      while( it != ite ) s.push_back( *it++ );
      return s;
    }

    vector<T> arc( const vector<int>& radius ) const
    {
      int size = 6 * accumulate(radius.begin(), radius.end(), 0 );
      vector<T> s( size );
      for_each( radius.begin(), radius.end(), 
      [&,i=0] (const int r) mutable
      {
          const vector<T> x = arc(r);
          copy( x.begin(), x.end(), s.begin() + i*6 );
          i+=r;
      });
      return s;
    }

    friend bool operator==( const cell<T>& c1, const cell<T>& c2 )
    {
      return c1.id == c2.id && c1._board == c2._board;
    }
    friend bool operator!=( const cell<T>& c1, const cell<T>& c2 )
    {
      return c1 != c2;
    }

 public:
    board_ref _board;
    cell_id id;
};


template< typename T, 
          typename _ = enable_if< is_base_of<cell<T>, T>::value >>
constexpr unique_ptr<board<T>> make_board() 
{
    unique_ptr<board<T>> b = make_unique<board<T>>();
    for_each( b->cells.begin(), b->cells.end(), 
    [&b]( T& c ) { c._board = b.get(); });
    return b;
}
template< typename T, 
          typename _ = enable_if< is_base_of<cell<T>, T>::value >>
constexpr unique_ptr<board<T>> copy_board( const board<T>& _b) 
{
    unique_ptr<board<T>> b = make_unique<board<T>>(_b);
    for_each( b->cells.begin(), b->cells.end(), 
    [&b]( T& c ) { c._board = b.get(); });
    return b;
}


struct forest;

struct land : public cell<land>
{
    static board<land> main_board;

    constexpr land( const int id = -1, const int r = 0 ): 
      cell<land>(), richness() 
    { 
      _board = &main_board;
    }
    land( const land& oth ):
      cell<land>(oth.id), richness(oth.richness)
    {
      _board =  oth._board;
    }

    friend istream& 
    operator>>( istream& in, land& l )
    {
      return in >> l.id >> l.richness;
    }
    friend ostream& 
    operator<<( ostream& out, const land& l )
    {
      out << "land #" << l.id << " coord: " << main_board.coord(l.id);
      return out << " richness: " << l.richness << endl;
    }

    int cut_points( const forest& f ) const; 
    vector<int> sunny_days( const forest& ) const;
    vector<int> permanent_sunny_days() const;
    vector<int> friendly_shadow( const forest& ) const;

public:
    int richness;
};

board<land> land::main_board = board<land>();


struct tree : public cell<tree>  
{
    constexpr static array<int,5> first_grow_cost =  { 0, 1, 3, 7, 4 };

public:
    constexpr tree(): cell<tree>(), size(-1), mine(), dormant() {} 
    constexpr tree( const cell_id i, bool m = true, const int s = 0, bool d = true ): 
      cell<tree>(i), size(s), mine(m), dormant(d) 
    {}
    constexpr tree( const tree& oth ):
      cell<tree>(oth.id), size(oth.size), mine(oth.mine), dormant(oth.dormant)
    {
      _board = oth._board;
    }

    friend istream& 
    operator>>( istream& in, tree& t )
    {
      return in >> t.id >> t.size >> t.mine >> t.dormant;
    }
    friend ostream&
    operator<<( ostream& out, const tree& t )
    {
      out << "tree #" << t.id << (t.id < 10 ? " ":"");
      if ( t.id == -1 ) return out << endl;
      out << " coord: " << t._board->coord(t.id) << " size: " <<  t.size; 
      out << " mine: " << boolalpha << t.mine << (t.mine ? " ":"") << " dormant: "<< t.dormant;
      return out << endl;
    }

    int grow_cost( const forest& f, const int s ) const;
    int grow_cost( const forest& f ) const { return grow_cost(f, size+1); }
    int cut_cost() const { return first_grow_cost[4]; }
    int seed_cost( const forest& f ) const { return grow_cost(f, 0); }

    // only effective shadow
    vector<tree> cast_shadow_to( const int day ) const;
    vector<tree> is_shadowed_by( const int day ) const;

    // includes level superior and inferior shadows
    vector<tree> almost_cast_shadow_to( const int day ) const;
    vector<tree> is_almost_shadowed_by( const int day ) const;

    vector<land> can_seed_on() const;

public:
    int size;
    bool mine, dormant;
};
       


/** 
 * Arithmetic
 */
using size_f = array<int,4>;

size_f operator+( size_f lhs, const size_f& rhs ) noexcept
{
    transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), 
    [](const int l, const int r ){ return l + r; });
    return lhs;
}
size_f operator-( size_f lhs, const size_f& rhs ) noexcept
{
    transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), 
    [](const int l, const int r ){ return l - r; });
    return lhs;
}
ostream& operator<<( ostream& out, const size_f& s )
{
    out << "[ ";
    copy( s.begin(), s.end()-1, ostream_iterator<int>(out, ", "));
    return out << s.back() << " ]";
}



/**
 *  Forest
 */

struct forest
{
    using size_f = array<int,4>;
    using cell_id = board_types::cell_id;
    using board_type = board<tree>;
    using board_ref = board<tree>&;

public:
    forest( board_type& b, const bool m, const int s = 2, 
            const int d = 0, const int n = 20, const int sc = 0 ): 
      mine(m), sun(s), day(d), nutrients(n), score(sc), _board(b), size(), 
      waiting(false)
    { 
      set_size();
    } 
    forest( const forest& f ):
      mine(f.mine), sun(f.sun), day(f.day), nutrients(f.nutrients), score(f.score),
      _board(f._board), size(f.size), waiting(f.waiting)
    {}
    
    void set_size()
    {
      vector<tree> m_trees = trees();
      generate( size.begin(), size.end(), 
      [&,i=0] () mutable 
      {
        return 
          count_if( m_trees.begin(), m_trees.end(), 
          [j=i++]( const tree& t ){ return t.size == j; });
      });
    }
    vector<tree> trees()
    {
      vector<tree> m_trees;
      copy_if( _board.cells.begin(), _board.cells.end(), back_inserter(m_trees),
      [&] ( const tree& t ) { return t.id > -1 && t.mine == mine; });
      return m_trees;
    }

    friend istream &
    operator>>( istream& in, forest& f)
    {
        if ( f.mine ) 
        {
          in >> f.day; in.ignore();
          in >> f.nutrients; in.ignore();
          in >> f.sun >> f.score; in.ignore();
        }
        else 
          in >> f.sun >> f.score >> f.waiting; in.ignore();
        return in;
    }
    friend ostream & 
    operator<<( ostream& out, const forest& s)
    {
        out << (s.mine ? 'm': 'o') << "_forest"; 
        out << " day: " << s.day << " sun: " << s.sun << " size: " << s.size;
        return out << " nutrients: " << s.nutrients << " score: " << s.score << endl;
    }

    int income( const int day ) const;
    forest& seed( tree& t, const land& l );
    forest& grow( tree& t );
    forest& cut( tree& t );
    forest& wait(); 

public:
    bool mine, waiting;
    int sun, day;
    int nutrients, score;
    board_ref _board;
    size_f size;
};



struct shadow 
{
  shadow& operator-=( const int n ) noexcept
  {
      transform( len.begin(), len.end(), len.begin(),  
      [&]( const int shd ){ return max(0, shd-n); });
      return *this;
  }
  shadow& operator--() noexcept
  {
      return *this-=1; 
  } 
  shadow operator--( int ) noexcept
  {
      shadow _shd = *this; 
      --*this;
      return _shd;
  }
  shadow& operator+=( const tree& t ) noexcept
  {
      --*this;
      if ( t.size <= 0 ) return *this;
      len[t.size-1] = t.size;
      return *this;
  }
  shadow operator+( const tree& t ) noexcept
  { 
      shadow shd = *this;
      return shd+=t; 
  }
  bool operator>=( const tree& t ) noexcept
  { 
      if (t.size <= 0) return false;
      return any_of( len.begin() + t.size - 1, len.end(), 
        []( const int l ){ return l > 0; }); 
  }

public:
  array<int, 3> len;
};


/**
 *  Land
 */

int land::cut_points( const forest& f ) const 
{ 
  return f.nutrients + (richness - 1)*2; 
}


vector<int> land::permanent_sunny_days() const
{
  vector<int> week(6);
  iota( week.begin(), week.end(), 0);
  vector<land> lands = range( week );

  week.erase( 
    remove_if( week.begin(), week.end(), 
    [&lands] (const int o) mutable
    {
      return 
          any_of( lands.begin() + 3*o, lands.begin() + 3*( o + 1 ), 
          []( const land& l){ return l.id != -1 && l.richness > 0; });
    }), week.end());
  return week;
}


vector<int> land::sunny_days( const forest& f ) const
{
  vector<int> week(6);
  iota( week.rbegin(), week.rend(), 0);
  vector<land> lands = range( week );
  reverse( lands.begin(), lands.end() );

  vector<tree> trees(lands.size());
  transform( lands.begin(), lands.end(), trees.begin(),
  [&f]( const land& l ) { return f._board[l.id]; });

  week.erase(
    remove_if( week.begin(), week.end(), 
    [&trees] ( const int o ) mutable
    {
      return accumulate( trees.begin() + 3*o, trees.begin() + 3*(o+1), 
             shadow())  >= tree(-1, true, 1);
    }), week.end());
  reverse( week.begin(), week.end() );
  return week;
}


vector<tree> land::friendly_shadow( const forest& f ) const
{
  vector<int> week(6);
  iota( week.rbegin(), week.rend(), 0);
  vector<land> lands = range( week );
  vector<tree> trees(lands.size());

  transform( lands.begin(), lands.end(), trees.begin(),
  [&f]( const land& l ) { return f._board[l.id]; });

  trees.erase(
    remove_if( trees.begin(), trees.end(), 
    [&trees,&f] ( const tree& t ) mutable
    {
      return t.id == -1 || t.mine != f.mine;
    }), trees.end());
  return trees;
}



/*
 *  Tree
 */


int tree::grow_cost( const forest& f, const int s ) const
{
  if ( s == 4 ) return cut_cost();
  return first_grow_cost[s] + f.size[s];
}

vector<tree> tree::cast_shadow_to( const int day ) const
{
    vector<tree> u = range( (day+3)%6, 2 );
    vector<tree> v = range( day%6, size );
    shadow shd = accumulate( u.rbegin(), u.rend(), shadow() );  
    --shd;
    v.erase( 
        remove_if( v.begin(), v.end(), 
        [&]( const tree& t )
        { 
          bool shadow = shd >= t;
          shd += t;
          return t.id == -1 || shadow || t.size > size; 
        }),
        v.end());
    return v;
}
vector<tree> tree::almost_cast_shadow_to( const int day ) const
{
    if ( size == 3 ) 
      return cast_shadow_to( day );
    tree t(*this);
    t.size += 1;
    return t.cast_shadow_to( day );
}

vector<tree> tree::is_shadowed_by( const int day ) const
{
  vector<tree> u = range( (day + 3)%6 );
  int i = 0;
  u.erase(
      remove_if( u.begin(), u.end(), 
      [&]( const tree& t ) mutable
      {  
        ++i;
        return t.id == -1 || t.size < max( i, size ); 
      }),
      u.end());
  return u;
}
vector<tree> tree::is_almost_shadowed_by( const int day ) const
{
  vector<tree> u = range( (day + 3)%6 );
  int i = 0;
  u.erase(
      remove_if( u.begin(), u.end(), 
      [&]( const tree& t ) mutable
      {  
        ++i;
        return t.id == -1 || t.size+1 < max( i, size ); 
      }),
      u.end());
  return u;
}
vector<land> tree::can_seed_on() const
{
  vector<int> rad(size);
  iota( rad.begin(), rad.end(), 1 );
  vector<land> u = land::main_board[this->id].arc( rad );
  u.erase( 
      remove_if( u.begin(), u.end(), 
      [&]( const land& l )
      { 
        return l.richness == 0 || _board->cells[l.id].id != -1; 
      }),
      u.end());
  return u;
}


          
/*
 *  Forest
 */
int forest::income( const int _day ) const
{
  using board_types::coordinate;
  using board_types::axis_value;
  array<coordinate, 7> start_pos;  

  generate( start_pos.begin(), start_pos.end(), 
  [x=-4, o=_day%6]() mutable -> coordinate
  {
    ++x;
    axis_value y = x < 0 ? 3 + x : 3;
    axis_value z = x < 0 ? 3 : 3 - x;  

    array<axis_value, 3> _p;
    array<axis_value, 6> p = { x, y, z, -x, -y, -z };

    rotate( p.rbegin(), p.rbegin() + o, p.rend() );
    copy( p.begin(), p.begin()+3, _p.begin() );
    return _p;
  });

  const auto line_income = [&]( const int _s, const coordinate& c )
  {
//    cerr << "line: " << c << endl;
    board_iterator<tree> it( _board, c, _day%6 );
//    cerr << "end:  " << (--it.end()).pos << endl;
    return  _s +
        accumulate( it, it.end(), 0,
        [&, shd=shadow()]( const int s, const tree& t ) mutable
        {
          bool _shd = shd >= t;
          shd += t;
          return s + ( t.id > -1 && t.mine == mine && !_shd ? t.size : 0 );
        });
  };

  return accumulate( start_pos.begin(), start_pos.end(), 0, line_income);
}




/**
 * Error
 **/

#ifdef DEFENSIVE

struct misplay  
{
    template<typename ...Args>
    misplay(Args... message) { 
        print(message...); 
    }
    template<typename T, typename ...Args>
    void print(T a, Args ...b) { 
        cerr << a; 
        print(b...);
    }
    template<typename T> 
    void print(T a){
       cerr << a; 
    }
};

#endif


forest& forest::seed( tree& t, const land& l )
{
#ifdef DEFENSIVE
  if ( l.richness == 0 || _board[l.id].id != -1 ) 
  {
    auto [x,y,z] = _board.coord(l.id);
    throw misplay("No seed can grow in position x:", x, " y:", y, " z:", z);
  }
  if ( t.dormant )
    throw misplay("Tree is dormant, id:", t.id);

  int c = t.seed_cost(*this);
  if ( sun < c )
    throw misplay("Not enough sun points, current:", sun, " required:", c);
  sun -= c;
#else
  sun -= t.seed_cost(*this);
#endif
  t.dormant = true;
  ++size[0];
  _board[l.id] = tree( l.id, mine );
  return *this;
}


forest& forest::grow( tree& t )
{
#ifdef DEFENSIVE
  if ( t.size == 3 ) 
    throw misplay("Tree cannot grow to size > 3");

  if ( t.dormant )
    throw misplay("Tree is dormant, id:", t.id);

  int c = t.grow_cost(*this);
  if ( sun < c )
    throw misplay("not enough sun points current:", sun, " required:", c);
  sun -= c;
#else
  sun -= t.grow_cost( *this );
#endif
  t.dormant = true;
  --size[t.size++];
  ++size[t.size]; 
  return *this;
}

forest& forest::cut( tree& t )
{
#ifdef DEFENSIVE
  if ( t.size < 3 )
    throw misplay("Tree smaller than 3 cannot be cut, size:", t.size);

  if ( t.dormant )
    throw misplay("Tree is dormant, id:", t.id);

  int c = t.cut_cost();
  if ( sun < c )
    throw misplay("not enough sun points current:", sun, " required:", c);
  sun -= c;
#else
  sun -= t.cut_cost();
#endif
  score += land::main_board[t.id].cut_points(*this);
  --nutrients;
  --size[3];
  _board[t.id] = tree();
  return *this;
}

forest& forest::wait() 
{
  sun += income( ++day );
  cerr << "days wealth: " << sun << endl;
  for( const tree& t : trees() )
    _board[t.id].dormant = false;
  return *this;
}




#endif /* __ALGO_BASE__ */

