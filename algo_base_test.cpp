#include <iostream>
#include <sstream>

#include "algo_base.cpp"




void try_range_arc( board<land>& bl, board<tree>& _bt )
{


  vector<land> l1 = bl[1].range(5);
  cerr << "l1:" << endl;
  copy( l1.begin(), l1.end(), ostream_iterator<land>(cerr) );

  vector<tree> r0 = _bt[1].range({ 5, 0, 1});
  cerr << "r0:" << endl;
  copy( r0.begin(), r0.end(), ostream_iterator<tree>(cerr) );

  vector<tree> r1 = _bt[19].arc({1,2});
  cerr << "r1:" << endl;
  copy( r1.begin(), r1.end(), ostream_iterator<tree>(cerr) );
}


void try_shadow_and_seed( board<land>& bl, board<tree>& bt, forest& m_forest )
{
  vector<int> sp19 = bl[19].permanent_sunny_days();
  cerr << "permanent sunny days #19: ";
  copy( sp19.begin(), sp19.end(), ostream_iterator<int>(cerr, ", ") );
  cerr << endl;

  vector<int> s0 = bl[0].sunny_days( m_forest );
  cerr << "sunny days #0: ";
  copy( s0.begin(), s0.end(), ostream_iterator<int>(cerr, ", ") );
  cerr << endl;

  vector<int> s19 = bl[0].sunny_days( m_forest );
  cerr << "sunny days #19: ";
  copy( s0.begin(), s0.end(), ostream_iterator<int>(cerr, ", ") );
  cerr << endl;


  vector<tree> ah17d0 = bt[17].is_almost_shadowed_by(0);
  cerr << "#17 almost in shadow of, d0: ";
  copy( ah17d0.begin(), ah17d0.end(), ostream_iterator<tree>(cerr) );

  vector<tree> ah1d0 = bt[1].is_almost_shadowed_by(0);
  cerr << "#1 almost in shadow of, d0: ";
  copy( ah1d0.begin(), ah1d0.end(), ostream_iterator<tree>(cerr) );
  cerr << endl;


  vector<tree> h17d3 = bt[17].is_almost_shadowed_by(3);
  cerr << "#17 shadowed by, d3: ";
  copy( h17d3.begin(), h17d3.end(), ostream_iterator<tree>(cerr) );

  vector<tree> h17d0 = bt[17].is_almost_shadowed_by(0);
  cerr << "#17 shadowed by7, d0: ";
  copy( h17d0.begin(), h17d0.end(), ostream_iterator<tree>(cerr) );
  cerr << endl;


  vector<tree> t7d3 = bt[7].cast_shadow_to(3);
  cerr << "#7  shadows to d3: ";
  copy( t7d3.begin(), t7d3.end(), ostream_iterator<tree>(cerr) );

  vector<tree> t17d3 = bt[17].cast_shadow_to(3);
  cerr << "#17 shadows to d3: ";
  copy( t17d3.begin(), t17d3.end(), ostream_iterator<tree>(cerr) );
  cerr << endl;

  vector<tree> at17d3 = bt[17].almost_cast_shadow_to(3);
  cerr << "#17 almost cast shadow to d3: ";
  copy( at17d3.begin(), at17d3.end(), ostream_iterator<tree>(cerr) );
  cerr << endl;

  vector<tree> at17d2 = bt[17].almost_cast_shadow_to(2);
  cerr << "#17 almost cast shadow to d2: ";
  copy( at17d2.begin(), at17d2.end(), ostream_iterator<tree>(cerr) );
  cerr << endl;


  vector<land> sd1 = bt[1].can_seed_on();
  cerr << "#1 can seed: ";
  copy( sd1.begin(), sd1.end(), ostream_iterator<land>(cerr) );
  cerr << endl;


}


int main()
{
  board<land>& bl = land::main_board;
  unique_ptr<board<tree>> p_bt = make_board<tree>();
  board<tree>& bt = *p_bt;
  for ( int i = 0; i < 37; ++i )
  {
    stringstream ssl, sst;
    ssl << i << " " << 1;
    ssl >> bl[i];
    if ( i % 2 )
    {
      sst << i << " " << 1 << " " << (bool)(i%4 == 1) << " " << false;
      sst >> bt[i];
    }
  }
  
  unique_ptr<board<tree>> _p_bt = copy_board(bt);
  board<tree>& _bt = *_p_bt;

  cerr << _bt[1] << _bt[25] << _bt[24]; 

  forest m_forest( _bt, true ), o_forest( _bt, false );
  cerr << m_forest << o_forest << endl;

  //try_range_arc( bl, _bt ); 
  
  //try_shadow_and_seed(bl, bt);



  /*
  cerr << "\nday 0" << endl;
  m_forest.income(0);
  //cerr << "o income:" << o_forest.income(0) << endl;

  cerr << "\nday 1" << endl;
  m_forest.income(1);

  cerr << "\nday 2" << endl;
  m_forest.income(2);

  cerr << "\nday 3" << endl;
  m_forest.income(3);

  cerr << "\nday 4" << endl;
  m_forest.income(4);

  cerr << "\nday 5" << endl;
  m_forest.income(5);
  */
  





  cerr << "m_forest wait" << endl;
  cerr << m_forest.wait() << endl;


  cerr << "m_forest grow #1" << endl;
  cerr << m_forest.grow( m_forest._board[1] );
  cerr << m_forest._board[1] << endl;


  cerr << "m_forest seed #5 -> #0" << endl;
  cerr << m_forest.seed( m_forest._board[5], land::main_board[0] );
  cerr << m_forest._board[5];
  cerr << m_forest._board[0] << endl;

  m_forest.wait();
  cerr << "m_forest grow #1" << endl;
  cerr << m_forest.grow( m_forest._board[1] );
  cerr << m_forest._board[1] << endl;

  m_forest.wait();
  cerr << "m_forest cut #1" << endl;
  cerr << m_forest.cut( m_forest._board[1] );
  cerr << m_forest._board[1] << endl;


}
