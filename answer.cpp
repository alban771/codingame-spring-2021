#include <iostream>
#include <vector>
#include <variant>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <memory>
#include <iterator>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <unordered_map>


using namespace std;


#ifndef _BOARD_
#define _BOARD_


/* board with 3 axes coordinates: ^x (north), <\y (west-north-west), </z (west-south-west)
*/                                           
namespace board_types 
{
    typedef int axis_value; 
    typedef int cell_id; 
    typedef int orientation; 
    typedef array<axis_value, 3>    coordinate;   // x,y,z with z = y-x
    typedef array<coordinate, 37>   coords_array; 
    typedef array<cell_id, 37>      index_array;
    typedef array<cell_id, 7>       offset_array;
};


ostream& operator<<( ostream& out, const board_types::coordinate& c )
{
    out << (c[0]>=0 ? " ":"") << c[0] << ", ";
    out << (c[1]>=0 ? " ":"") << c[1] << ", ";
    return out << (c[2]>=0 ? " ":"") << c[2];
}

constexpr auto _abs = []( const int i ){ return i < 0 ? -i : i; };

// compute the coordinates of every cell
constexpr auto board_coords = []()
{
    using board_types::coords_array;
    using board_types::coordinate;

    const auto _pos = []( const int a, const int b, const int c) 
    {
        return [=]( const int d )
        {
            return  ( a*_abs( (b-d+4)%3 - 1 ) - c*( (b-d+3)%3 - 1 )) * 
                ( 0 <= (b-d) && (b-d) < 3 ? 1 : -1);
        };
    };

    coords_array coords = {};
    coords[0] = {0,0,0};
    auto it = coords.begin() + 1; 
    for ( int a = 1; a < 4; ++a )
    {
        auto ite = it + a*6;
        for ( int k=0; it != ite; ++it, ++k )
        {
            int b = k / a, c = k % a;
            const auto pos = _pos(a,b,c); 
            *it = coordinate({ pos(0), pos(1), pos(2) });
        };
    }
    return coords;
};




// conversely sort indexes by coordinates 
constexpr auto board_index = []( const board_types::coords_array& coords )
{
    using board_types::coords_array;
    using board_types::index_array;
    using board_types::coordinate;
    using coord_id = array<int, 4>;

    index_array index = {0};
    array<coord_id, 37> sorted = {}; // x,y,z,id

    for( int i = 0; i < coords.size(); ++i )
    {
        const auto [x,y,z] = coords[i];
        sorted[i] = { x,y,z,i };
    } 
    const auto compare = []( coord_id& c1, coord_id& c2 )
    {
        return c1[0] < c2[0] || ( c1[0] == c2[0] && c1[1] < c2[1] );
    };
    for ( int i = 1; i < sorted.size(); ++i)
    {
        int j = i;
        while( j > 0 && compare(sorted[j], sorted[j-1]) )
        {
            auto tmp = move( sorted[j] ); 
            sorted[j] = move( sorted[j-1] );
            sorted[--j] = move( tmp );
        }
    }
    for ( int i = 0; i < sorted.size(); ++i)
        index[i] = sorted[i][3];

    return index;
};



// coordinate ranges do not have same number of elements  
// compute offset values for random coordinate access   
constexpr auto board_offset = []()
{
    using board_types::offset_array;

    offset_array offset = {};
    for ( int x = -3; x < 4; ++x )
        offset[x+3] = 18 + 6*x - x*(_abs(x)-1)/2;
    return offset;
};



template< typename T > 
struct board 
{
    using axis_value = board_types::axis_value;
    using cell_id = board_types::cell_id;
    using coords_array = board_types::coords_array; 
    using index_array = board_types::index_array; 
    using offset_array = board_types::offset_array;
    using coordinate = const board_types::coordinate;
    typedef T value_type;
    typedef T& reference;

    static constexpr coords_array coords = board_coords();
    static constexpr index_array  index  = board_index( coords ); 
    static constexpr offset_array offset = board_offset();

    constexpr board(): cells() {}
    constexpr board( const board<T>& b ): cells(b.cells) {}
    constexpr board( board<T>&& b ): cells( move(b.cells) ) {}

    reference operator[]( const cell_id i )
    {
        return cells[i];
    }

    coordinate& coord( const cell_id i ) const
    {
        return coords[i];
    }

    reference at( coordinate& c ) 
    {
        const auto [x,y,z] = c;
        return at(x,y,z);
    }

    reference at(const axis_value x, const axis_value y, const axis_value z) 
    {
        assert( z == y-x );
        return cells[ index[ offset[x+3] + y ] ];
    }

    public:
    array<value_type, 37>   cells;
};




/* random access iterator. 
   e.g. range from one cell: 
   auto it0 = board_iterator(board, 0, cell_id);
   for ( auto it = it0 - 3; it != it0 + 3; ++it )
   */
template<typename T>
struct board_iterator 
{
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T&;
    using iterator_category = std::random_access_iterator_tag;

    using board_type = board<T>&;
    using coordinate = board_types::coordinate;
    using cell_id = board_types::cell_id;
    using orientation = board_types::orientation;


    constexpr board_iterator( board_type b, const cell_id i, const orientation o ) noexcept: 
        _board(b), ori(o), pos(_board.coord(i))
        {}
    constexpr board_iterator( board_type b, const coordinate& p, const orientation o ) noexcept: 
        _board(b), ori(o), pos(p)
        {}
    constexpr board_iterator( const board_iterator& it ) noexcept:
        _board(it._board), ori(it.ori), pos(it.pos)
        {}

    constexpr board_iterator end() const noexcept
    {
        auto [x,y,z] = pos;
        int u = 6, v = u, w = v;
        if (ori % 3 != 0 ) u =  ori      > 3 ? 3+x : 3-x;
        if (ori % 3 != 1 ) v = (ori+2)%6 > 3 ? 3-y : 3+y;
        if (ori % 3 != 2 ) w = (ori+1)%6 > 3 ? 3-z : 3+z;
        return *this + min(u, min(v,w)) + 1;
    }

    constexpr reference operator*() const noexcept 
    {
        return _board.at(pos);
    }
    constexpr pointer operator->() const noexcept 
    {
        return &_board.at(pos);
    }
    constexpr value_type operator[](const difference_type n) const noexcept
    {
        auto [x,y,z] = pos;
        if (ori % 3 != 0 ) x +=  ori      > 3 ? -n : n;
        if (ori % 3 != 1 ) y += (ori+2)%6 > 3 ?  n :-n;
        if (ori % 3 != 2 ) z += (ori+1)%6 > 3 ?  n :-n;
        return _board.at(x,y,z);
    }

    constexpr board_iterator & operator+=(const difference_type n) noexcept
    {
        auto& [x,y,z] = pos;
        if (ori % 3 != 0 ) x +=  ori      > 3 ? -n : n;
        if (ori % 3 != 1 ) y += (ori+2)%6 > 3 ?  n :-n;
        if (ori % 3 != 2 ) z += (ori+1)%6 > 3 ?  n :-n;
        return *this;
    }
    constexpr board_iterator & operator++() noexcept
    {
        return *this += 1;
    }
    constexpr board_iterator operator++(int)noexcept
    {
        board_iterator retval = *this;
        ++*this;
        return retval;
    }
    friend constexpr board_iterator 
        operator+(board_iterator lhs, const difference_type n) noexcept
        {
            return lhs += n;
        }

    constexpr board_iterator & operator-=(const difference_type n) noexcept
    {
        return *this += -n;
    }
    constexpr board_iterator & operator--() noexcept
    {
        return *this -= 1;
    }
    constexpr board_iterator operator--(int)noexcept
    {
        board_iterator retval = *this; 
        --*this;
        return retval;
    }
    friend constexpr board_iterator 
        operator-(board_iterator lhs, const difference_type n) noexcept
        {
            return lhs -= n;
        }

    friend constexpr difference_type 
        operator-(const board_iterator& lhs, const board_iterator& rhs) 
        {
            assert( ( lhs.ori - rhs.ori ) % 6 == 0 );  
            const auto [lx,ly,lz] = lhs.pos;
            const auto [rx,ry,rz] = rhs.pos;
            assert( lhs.ori % 3 != 0 || lx == rx );  
            assert( lhs.ori % 3 != 1 || ly == ry );  
            assert( lhs.ori % 3 != 2 || lz == rz );  
            return  lhs.ori % 3 != 0 ? 
                lhs.ori < 3 ? lx - rx : rx - lx : 
                lhs.ori > 0 ? ly - ry : ry - ly;
        } 

    friend constexpr bool operator==(
            const board_iterator& lhs, const board_iterator& rhs) noexcept
    {
        return ( lhs.ori - rhs.ori )%6 == 0 && lhs.pos == rhs.pos;
    }
    friend constexpr bool operator!=(
            const board_iterator& lhs, const board_iterator& rhs) noexcept
    {
        return !(lhs == rhs);
    }

    public:
    board_type _board;
    orientation ori;
    coordinate pos; 

};


/* circular board iterator. 
   e.g. arc of radius 2 centered on one cell: 
   auto it0 = arc_iterator(board, cell_id, 2);
   for ( auto it = it0; it != it0.end(); ++it )
   */
template<typename T>
struct arc_iterator : public board_iterator<T>
{
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T&;
    using iterator_category = std::bidirectional_iterator_tag;

    using coordinate = board_types::coordinate;
    using cell_id = board_types::cell_id;
    using orientation = board_types::orientation;
    using board_type = board<T>&;

    using array_limits = array<difference_type,7>;


    constexpr array_limits find_limits() const noexcept 
    {
        array_limits l;
        board_iterator it(this->_board, center, 0); 
        generate( l.begin(), l.end() - 1, 
                [&,o=0]() mutable
                { 
                it.ori = o++;
                return min( it.end() - it - 1, radius );
                });
        l[6] = l[0];
        return l;
    }

    template< bool clockwise = false, 
        typename It1 = array_limits::iterator,
        typename It2 = array_limits::reverse_iterator,
        typename Cond = conditional_t< clockwise, It1, It2 > >
            constexpr void init() noexcept
            {
                array_limits limits = find_limits();

                variant<It1,It2> vlstart, vlend;
                if ( clockwise )
                    vlstart = limits.begin(),  vlend = limits.end();
                else 
                    vlstart = limits.rbegin(), vlend = limits.rend();
                Cond lstart = get<Cond>(vlstart), lend = get<Cond>(vlend);
                Cond _lstart = find_if( lstart, lend, [&](const int li) { return li >= radius; });

                Cond f = find_if( _lstart + ( lstart == _lstart ? 1 : 0 ) , lend, 
                        [&]( const difference_type i)  
                        {
                        return i < radius;
                        });
                --f;

                int a0 = 0, a1 = 0, a2 = 0, &ori = this->ori;
                if ( clockwise )
                    a0 = f - lstart,     a1 = (6 + a0 - 1)%6, a2 = (a0 + 1)%6; 
                else
                    a0 = lstart - f + 6, a1 = (a0 + 1)%6, a2 = (6 + a0 - 1)%6; 

                ori = a0;
                this->pos = center;
                this->board_iterator<T>::operator+=(radius);

                ori = a1;
                tan = radius;
                if ( ++f != lend ) tan -= limits[a2];
                this->board_iterator<T>::operator-=(radius - tan);
            }

    public:
    constexpr arc_iterator( board_type b, const coordinate& c, const difference_type r ) noexcept:
        board_iterator<T>(b, 0, c), center(c), radius(r), _start(true)
        {
            init();
        }
    constexpr arc_iterator( board_type b, const cell_id i, const difference_type r ) noexcept:
        board_iterator<T>(b, 0, i), center(b.coord(i)), radius(r), _start(true)
        {
            init();
        }
    constexpr arc_iterator( const arc_iterator& oth ) noexcept:
        board_iterator<T>(oth), center(oth.center), radius(oth.radius)
        {}
    constexpr arc_iterator end() noexcept
    {
        arc_iterator it = *this;
        it.init<true>();
        if ( it.pos == this->pos )
        {
            it.ori += 2;
            it.ori %= 6;
            it._start = false;
        }
        else
        {
            it.ori += 3;
            it.ori %= 6;
            it.tan = radius - it.tan;
            ++it;
        }
        return it;
    }

    constexpr value_type operator[](const difference_type n) const = delete;
    constexpr arc_iterator & operator+=( const difference_type n ) = delete;
    constexpr arc_iterator & operator++() noexcept
    {
        if ( tan == radius ) 
        { 
            this->ori += 1;
            this->ori %= 6;
            tan = 0;
        }
        ++tan;
        _start = false;
        this->board_iterator<T>::operator++();
        return *this;
    }
    constexpr arc_iterator operator++(int) noexcept
    {
        arc_iterator it = *this;
        ++*this;
        return it;
    }

    constexpr arc_iterator & operator-=( const difference_type n ) = delete;
    constexpr arc_iterator & operator--() noexcept
    {
        if ( tan == 0 ) 
        { 
            this->ori += 5;
            this->ori %= 6;
            tan = radius;
        }
        --tan;
        _start = true;
        this->board_iterator<T>::operator--();
        return *this;
    }
    constexpr arc_iterator operator--(int) noexcept
    {
        arc_iterator it = *this;
        --*this;
        return it;
    }

    friend constexpr arc_iterator 
        operator+(arc_iterator lhs, const difference_type n) = delete;
    friend constexpr arc_iterator 
        operator-(arc_iterator lhs, const difference_type n) = delete;
    friend constexpr difference_type 
        operator-(const arc_iterator& lhs, const arc_iterator& rhs) = delete; 
    friend constexpr bool operator==(
            const arc_iterator& lhs, const arc_iterator& rhs) noexcept
    {
        return lhs.pos    == rhs.pos && 
            lhs.center == rhs.center && 
            lhs.ori    == rhs.ori &&
            lhs._start == rhs._start; // differentiate end() from begin() in circle;
    }
    friend constexpr bool operator!=(
            const arc_iterator& lhs, const arc_iterator& rhs) noexcept
    {
        return !( lhs == rhs );
    }

    public:
    const coordinate center;
    const difference_type radius;
    difference_type tan;
    bool _start;
};


#endif /* __BOARD__ */

#ifndef _ALGOBASE_
#define _ALGOBASE_
#define DEFENSIVE


using namespace std;


template<typename T>
struct cell 
{
    using board_type = board<T>;
    using board_ref = board_type *;
    using cell_id = board_types::cell_id;
    using orientation = board_types::orientation;
    using difference_type = typename board_iterator<T>::difference_type;

    public:
    constexpr cell(const cell_id i = -1): _board(nullptr), id(i) {}

    const auto begin() const
    {
        return _board->cells.begin();
    }
    const auto end() const
    {
        return _board->cells.end();
    }

    vector<T> range( const orientation ori, const difference_type dist = 3 ) const
    {
        board_iterator<T> it( *_board, id, ori ), ite = it.end();
        vector<T> s( min( (ite-it)-1, dist ) ); // exclude 0
        generate( s.begin(), s.end(), [&it]()  
                {
                return *(++it);
                });
        return s;
    }

    vector<T> range( const vector<orientation>& ori, const difference_type dist = 3 ) const
    {
        vector<T> s(dist * ori.size());
        for_each( ori.begin(), ori.end(), 
                [&,i=0] (const int o) mutable
                {
                const vector<T> r = range(o, dist);
                copy(r.begin(), r.end(), s.begin() + dist*i++ );
                });
        return s;
    }

    vector<T> arc( const int radius = 2 ) const
    {
        vector<T> s;
        arc_iterator<T> it( *_board, id, radius ), ite = it.end();
        cerr << "arc: " << it.pos << " end: " << ite.pos << endl;
        while( it != ite ) s.push_back( *it++ );
        return s;
    }

    vector<T> arc( const vector<int>& radius ) const
    {
        int size = 6 * accumulate(radius.begin(), radius.end(), 0 );
        vector<T> s( size );
        for_each( radius.begin(), radius.end(), 
                [&,i=0] (const int r) mutable
                {
                const vector<T> x = arc(r);
                copy( x.begin(), x.end(), s.begin() + i*6 );
                i+=r;
                });
        return s;
    }

    friend bool operator==( const cell<T>& c1, const cell<T>& c2 )
    {
        return c1.id == c2.id && c1._board == c2._board;
    }
    friend bool operator!=( const cell<T>& c1, const cell<T>& c2 )
    {
        return c1 != c2;
    }

    public:
    board_ref _board;
    cell_id id;
};


template< typename T, 
    typename _ = enable_if< is_base_of<cell<T>, T>::value >>
constexpr unique_ptr<board<T>> make_board() 
{
    unique_ptr<board<T>> b = make_unique<board<T>>();
    for_each( b->cells.begin(), b->cells.end(), 
            [&b]( T& c ) { c._board = b.get(); });
    return b;
}
template< typename T, 
    typename _ = enable_if< is_base_of<cell<T>, T>::value >>
constexpr unique_ptr<board<T>> copy_board( const board<T>& _b) 
{
    unique_ptr<board<T>> b = make_unique<board<T>>(_b);
    for_each( b->cells.begin(), b->cells.end(), 
            [&b]( T& c ) { c._board = b.get(); });
    return b;
}


struct forest;
struct tree;

struct land : public cell<land>
{
    static board<land> main_board;

    constexpr land( const int id = -1, const int r = 0 ): 
        cell<land>(), richness() 
    { 
        _board = &main_board;
    }
    land( const land& oth ):
        cell<land>(oth.id), richness(oth.richness)
    {
        _board =  oth._board;
    }

    friend istream& 
        operator>>( istream& in, land& l )
        {
            in >> l.id >> l.richness;
            int sink;
            for ( int _=0; _<6; ++_ )  cin >> sink;
            in.ignore();
            return in;
        }
    friend ostream& 
        operator<<( ostream& out, const land& l )
        {
            out << "land #" << l.id << " coord: " << main_board.coord(l.id);
            return out << " richness: " << l.richness << endl;
        }

    int cut_points( const forest& f ) const; 
    vector<int> sunny_days( const forest& ) const;
    vector<int> permanent_sunny_days() const;
    vector<tree> friendly_shadow(const forest& ) const;

    public:
    int richness;
};

board<land> land::main_board = board<land>();


struct tree : public cell<tree>  
{
    constexpr static array<int,5> first_grow_cost =  { 0, 1, 3, 7, 4 };

    public:
    constexpr tree(): cell<tree>(), size(-1), mine(), dormant() {} 
    constexpr tree( const cell_id i, bool m = true, const int s = 0, bool d = true ): 
        cell<tree>(i), size(s), mine(m), dormant(d) 
    {}
    constexpr tree( const tree& oth ):
        cell<tree>(oth.id), size(oth.size), mine(oth.mine), dormant(oth.dormant)
    {
        _board = oth._board;
    }

    friend istream& 
        operator>>( istream& in, tree& t )
        {
            return in >> t.id >> t.size >> t.mine >> t.dormant;
        }
    friend ostream&
        operator<<( ostream& out, const tree& t )
        {
            out << "tree #" << t.id << (t.id < 10 ? " ":"");
            if ( t.id == -1 ) return out << endl;
            out << " coord: " << t._board->coord(t.id) << " size: " <<  t.size; 
            out << " mine: " << boolalpha << t.mine << (t.mine ? " ":"") << " dormant: "<< t.dormant;
            return out << endl;
        }

    int grow_cost( const forest& f, const int s ) const;
    int grow_cost( const forest& f ) const { return grow_cost(f, size+1); }
    int cut_cost() const { return first_grow_cost[4]; }
    int seed_cost( const forest& f ) const { return grow_cost(f, 0); }

    // only effective shadow
    vector<tree> cast_shadow_to( const int day ) const;
    vector<tree> is_shadowed_by( const int day ) const;

    // includes level superior and inferior shadows
    vector<tree> almost_cast_shadow_to( const int day ) const;
    vector<tree> is_almost_shadowed_by( const int day ) const;

    vector<land> can_seed_on() const;

    public:
    int size;
    bool mine, dormant;
};



/** 
 * Arithmetic
 */
using size_f = array<int,4>;

size_f operator+( size_f lhs, const size_f& rhs ) noexcept
{
    transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), 
            [](const int l, const int r ){ return l + r; });
    return lhs;
}
size_f operator-( size_f lhs, const size_f& rhs ) noexcept
{
    transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), 
            [](const int l, const int r ){ return l - r; });
    return lhs;
}
ostream& operator<<( ostream& out, const size_f& s )
{
    out << "[ ";
    copy( s.begin(), s.end()-1, ostream_iterator<int>(out, ", "));
    return out << s.back() << " ]";
}



/**
 *  Forest
 */

struct forest
{
    using size_f = array<int,4>;
    using cell_id = board_types::cell_id;
    using board_type = board<tree>;
    using board_ref = board<tree>*;

    public:
    forest( board_ref b, const bool m, const int s = 10, 
            const int d = 10, const int n = 10, const int sc = 10 ): 
        mine(m), sun(s), day(d), nutrients(n), score(sc), _board(b), size(), 
        waiting(false), opponent(nullptr)
    { 
        set_size();
    } 
    forest( const forest& f ):
        mine(f.mine), sun(f.sun), day(f.day), nutrients(f.nutrients), score(f.score),
        _board(f._board), size(f.size), waiting(f.waiting), opponent(f.opponent)
    {}

    void set_size()
    {
        generate( size.begin(), size.end(), 
                [&,i=0] () mutable 
                {
                return 
                        count_if( _board->cells.begin(), _board->cells.end(), 
                        [&,j=i++]( const tree& t ){ 
                            return  t.id > -1 &&
                                    t.mine == mine &&
                                    t.size == j; 
                        });
                });
    }
    vector<tree> trees()
    {
        vector<tree> m_trees;
        copy_if( _board->cells.begin(), _board->cells.end(), back_inserter(m_trees),
                [&] ( const tree& t ) {  return t.id > -1 && t.mine == mine;  });
        return m_trees;
    }

    friend istream &
        operator>>( istream& in, forest& f)
        {
            if ( f.mine ) 
            {
                in >> f.day; 
                in >> f.nutrients; 
                in >> f.sun >> f.score; 
            }
            else 
                in >> f.sun >> f.score >> f.waiting; 
            return in;
        }
    friend ostream & 
        operator<<( ostream& out, const forest& s)
        {
            out << (s.mine ? 'm': 'o') << "_forest"; 
            out << " day: " << s.day << " sun: " << s.sun << " size: " << s.size;
            return out << " nutrients: " << s.nutrients << " score: " << s.score << endl;
        }

    int income( const int day ) const;
    forest& seed( tree& t, const land& l );
    forest& grow( tree& t );
    forest& cut( tree& t );
    forest& wait(); 

public:
    bool mine, waiting;
    int sun, day;
    int nutrients, score;
    board_ref _board;
    size_f size;
    forest* opponent;
};



struct shadow 
{
    shadow& operator-=( const int n ) noexcept
    {
        transform( len.begin(), len.end(), len.begin(),  
                [&]( const int shd ){ return max(0, shd-n); });
        return *this;
    }
    shadow& operator--() noexcept
    {
        return *this-=1; 
    } 
    shadow operator--( int ) noexcept
    {
        shadow _shd = *this; 
        --*this;
        return _shd;
    }
    shadow& operator+=( const tree& t ) noexcept
    {
        --*this;
        if ( t.size <= 0 ) return *this;
        len[t.size-1] = t.size;
        return *this;
    }
    shadow operator+( const tree& t ) noexcept
    { 
        shadow shd = *this;
        return shd+=t; 
    }
    bool operator>=( const tree& t ) noexcept
    { 
        if (t.size <= 0) return false;
        return any_of( len.begin() + t.size - 1, len.end(), 
                []( const int l ){ return l > 0; }); 
    }

    public:
    array<int, 3> len;
};


/**
 *  Land
 */

int land::cut_points( const forest& f ) const 
{ 
    return f.nutrients + (richness - 1)*2; 
}


vector<int> land::permanent_sunny_days() const
{
    vector<int> week(6);
    iota( week.begin(), week.end(), 0);
    vector<land> lands = range( week );

    week.erase( 
            remove_if( week.begin(), week.end(), 
                [&lands] (const int o) mutable
                {
                return 
                any_of( lands.begin() + 3*o, lands.begin() + 3*( o + 1 ), 
                        []( const land& l){ return l.id != -1 && l.richness > 0; });
                }), week.end());
    return week;
}


vector<int> land::sunny_days( const forest& f ) const
{
    vector<int> week(6);
    iota( week.rbegin(), week.rend(), 0);
    vector<land> lands = range( week );
    reverse( lands.begin(), lands.end() );

    vector<tree> trees(lands.size());
    transform( lands.begin(), lands.end(), trees.begin(),
            [&f]( const land& l ) 
            { 
                return l.id > -1 ? (*f._board)[l.id] : tree(); 
            });

    week.erase(
            remove_if( week.begin(), week.end(), 
                [&trees] ( const int o ) mutable
                {
                return accumulate( trees.begin() + 3*o, trees.begin() + 3*(o+1), 
                        shadow())  >= tree(-1, true, 1);
                }), week.end());
    reverse( week.begin(), week.end() );
    return week;
}


vector<tree> land::friendly_shadow( const forest& f ) const
{
  vector<int> week(6);
  iota( week.rbegin(), week.rend(), 0);
  vector<land> lands = range( week );
  vector<tree> trees(lands.size());

  transform( lands.begin(), lands.end(), trees.begin(),
  [&f]( const land& l ) 
  { 
      return l.id > -1 ? (*f._board)[l.id] : tree(); 
  });

  trees.erase(
    remove_if( trees.begin(), trees.end(), 
    [&trees,&f] ( const tree& t ) mutable
    {
      return t.id == -1 || t.mine != f.mine;
    }), trees.end());
  return trees;
}




/*
 *  Tree
 */


int tree::grow_cost( const forest& f, const int s ) const
{
    if ( s == 4 ) return cut_cost();
    return first_grow_cost[s] + f.size[s];
}

vector<tree> tree::cast_shadow_to( const int day ) const
{
    vector<tree> u = range( (day+3)%6, 2 );
    vector<tree> v = range( day%6, size );
    shadow shd = accumulate( u.rbegin(), u.rend(), shadow() );  
    --shd;
    v.erase( 
            remove_if( v.begin(), v.end(), 
                [&]( const tree& t )
                { 
                bool shadow = shd >= t;
                shd += t;
                return t.id == -1 || shadow || t.size > size; 
                }),
            v.end());
    return v;
}
vector<tree> tree::almost_cast_shadow_to( const int day ) const
{
    if ( size == 3 ) 
        return cast_shadow_to( day );
    tree t(*this);
    t.size += 1;
    return t.cast_shadow_to( day );
}

vector<tree> tree::is_shadowed_by( const int day ) const
{
    vector<tree> u = range( (day + 3)%6 );
    int i = 0;
    u.erase(
            remove_if( u.begin(), u.end(), 
                [&]( const tree& t ) mutable
                {  
                ++i;
                return t.id == -1 || t.size < max( i, size ); 
                }),
            u.end());
    return u;
}
vector<tree> tree::is_almost_shadowed_by( const int day ) const
{
    vector<tree> u = range( (day + 3)%6 );
    int i = 0;
    u.erase(
            remove_if( u.begin(), u.end(), 
                [&]( const tree& t ) mutable
                {  
                ++i;
                return t.id == -1 || t.size+1 < max( i, size ); 
                }),
            u.end());
    return u;
}
vector<land> tree::can_seed_on() const
{
    vector<int> rad(size);
    iota( rad.begin(), rad.end(), 1 );
    vector<land> u = land::main_board[this->id].arc( rad );
    u.erase( 
            remove_if( u.begin(), u.end(), 
                [&]( const land& l )
                { 
                return l.richness == 0 || _board->cells[l.id].id != -1; 
                }),
            u.end());
    return u;
}



/*
 *  Forest
 */
int forest::income( const int _day ) const
{
    using board_types::coordinate;
    using board_types::axis_value;
    array<coordinate, 7> start_pos;  

    generate( start_pos.begin(), start_pos.end(), 
            [x=-4, o=_day%6]() mutable -> coordinate
            {
            ++x;
            axis_value y = x < 0 ? 3 + x : 3;
            axis_value z = x < 0 ? 3 : 3 - x;  

            array<axis_value, 3> _p;
            array<axis_value, 6> p = { x, y, z, -x, -y, -z };

            rotate( p.rbegin(), p.rbegin() + o, p.rend() );
            copy( p.begin(), p.begin()+3, _p.begin() );
            return _p;
            });

    const auto line_income = [&]( const int _s, const coordinate& c )
    {
        //    cerr << "line: " << c << endl;
        board_iterator<tree> it( *_board, c, _day%6 );
        //    cerr << "end:  " << (--it.end()).pos << endl;
        return  _s +
            accumulate( it, it.end(), 0,
                    [&, shd=shadow()]( const int s, const tree& t ) mutable
                    {
                    bool _shd = shd >= t;
                    shd += t;
                    return s + ( t.id > -1 && t.mine == mine && !_shd ? t.size : 0 );
                    });
    };

    return accumulate( start_pos.begin(), start_pos.end(), 0, line_income);
}




/**
 * Error
 **/

#ifdef DEFENSIVE

struct misplay  
{
    template<typename ...Args>
        misplay(Args... message) { 
            print(message...); 
        }
    template<typename T, typename ...Args>
        void print(T a, Args ...b) { 
            cerr << a; 
            print(b...);
        }
    template<typename T> 
        void print(T a){
            cerr << a; 
        }
};

#endif


forest& forest::seed( tree& t, const land& l )
{
#ifdef DEFENSIVE
    if ( l.richness == 0 || (*_board)[l.id].id != -1 ) 
    {
        auto [x,y,z] = _board->coord(l.id);
        throw misplay("No seed can grow in position x:", x, " y:", y, " z:", z);
    }
    if ( t.dormant )
        throw misplay("Tree is dormant, id:", t.id);

    int c = t.seed_cost(*this);
    if ( sun < c )
        throw misplay("Not enough sun points, current:", sun, " required:", c);
    sun -= c;
#else
    sun -= t.seed_cost(*this);
#endif
    t.dormant = true;
    ++size[0];
    (*_board)[l.id] = tree( l.id, mine );
    return *this;
}


forest& forest::grow( tree& t )
{
#ifdef DEFENSIVE
    if ( t.size == 3 ) 
        throw misplay("Tree cannot grow to size > 3");

    if ( t.dormant )
        throw misplay("Tree is dormant, id:", t.id);

    int c = t.grow_cost(*this);
    if ( sun < c )
        throw misplay("not enough sun points current:", sun, " required:", c);
    sun -= c;
#else
    sun -= t.grow_cost( *this );
#endif
    t.dormant = true;
    --size[t.size++];
    ++size[t.size]; 
    return *this;
}

forest& forest::cut( tree& t )
{
#ifdef DEFENSIVE
    if ( t.size < 3 )
        throw misplay("Tree smaller than 3 cannot be cut, size:", t.size);

    if ( t.dormant )
        throw misplay("Tree is dormant, id:", t.id);

    int c = t.cut_cost();
    if ( sun < c )
        throw misplay("not enough sun points current:", sun, " required:", c);
    sun -= c;
#else
    sun -= t.cut_cost();
#endif
    score += land::main_board[t.id].cut_points(*this);
    --nutrients;
    --size[3];
    (*_board)[t.id] = tree();
    return *this;
}

forest& forest::wait() 
{
    sun += income( ++day );
    cerr << "days wealth: " << sun << endl;
    for( const tree& t : trees() )
        (*_board)[t.id].dormant = false;
    return *this;
}



#endif /* __ALGO_BASE__ */


#ifndef __DECISION__
#define __DECISION__

struct seed_st
{
    seed_st(const land& _l, const tree& t, const forest& _f ):
        l(_l), origin(1,t), f(_f), 
        friendly_shadow(_l.friendly_shadow(_f)),
        sunny_days(_l.sunny_days(_f))
    {}
    seed_st( const seed_st& s ):
        l(s.l), origin(s.origin), f(s.f),
        friendly_shadow(s.friendly_shadow),
        sunny_days(s.sunny_days)
    {}

    vector<int> shift_sunny( int offst ) const
    {        
        vector<int> sd(sunny_days.size());
        transform( sunny_days.begin(), sunny_days.end(), sd.begin(), 
        [&offst](const int i){ return (6 + i - offst%6)%6; });
        sort( sd.begin(), sd.end() );
        return sd;
    }

    friend bool
    operator<( const seed_st& l1, const seed_st& l2 )
    {
        if ( l1.friendly_shadow.size() == l2.friendly_shadow.size() )
        {
            vector<int> week (6), sd1 = l1.shift_sunny(l1.f.day+2), sd2 = l2.shift_sunny(l2.f.day+2);
            iota( week.begin(), week.end(), 0);
            
            auto [ _,f1] = mismatch( sd1.begin(), sd1.end(), week.begin() );
            auto [__,f2] = mismatch( sd2.begin(), sd2.end(), week.begin() );
            
            if ( f1 == f2 )
                return l1.l.richness < l2.l.richness;
            else
                return f1 < f2;
        }
        else
            return l1.friendly_shadow.size() > l2.friendly_shadow.size();
    }

public:
    land l; 
    forest f;
    vector<tree> origin;
    vector<tree> friendly_shadow;
    vector<int> sunny_days;
};


struct grow_st
{
    grow_st(const tree& _t, const forest& _f ):
        t(_t), f(_f),
        almost_shadowed_by(t.is_almost_shadowed_by( _f.day + 1 ))
        //almost_cast_shadow(t.almost_cast_shadow_to( _f.day + 1 ))
    {}
    grow_st(const grow_st& g ):
        t(g.t), f(g.f), 
        almost_shadowed_by(g.almost_shadowed_by),
        almost_cast_shadow(g.almost_cast_shadow)
    {}

    vector<tree> tail() const
    {
        vector<tree> tail;
        copy_if( almost_cast_shadow.begin(), almost_cast_shadow.end(), back_inserter(tail),
        [&]( const tree& _t )
        {
            return _t.size == t.size || _t.size == t.size + 1;
        });
        return tail;
    }
    tree head() const 
    {
        auto f = max_element( almost_shadowed_by.begin(), almost_shadowed_by.end(), 
        [](const tree& t1, const tree& t2 ){ return t1.size < t2.size; });
        if ( f == almost_shadowed_by.end() ) 
            return tree(-1);
        else
            return *f;
    }

    int makes() const
    {
        int s = 0;
        tree _head = head();
        const vector<tree> _tail = tail();

        cerr << "grow_st makes: " << t;
        cerr << "head: " << _head;


        //TODO: impl on forest
        const auto can_grow = 
        [opp=*f.opponent]( const vector<tree>& opp_t ) -> vector<bool>
        {
            vector<bool> cg(opp_t.size());

            transform( opp_t.begin(), opp_t.end(), cg.begin(), 
            [opp=opp,r=0]( tree __t) mutable
            {
                if (__t.id == -1 ) return false;
                if (__t.size == 3 ) return true;
                opp.sun += r == __t.size;
                r = __t.size+1;
                bool retval = ! ( __t.dormant || opp.waiting || __t.grow_cost(opp) >= opp.sun );
                if ( retval ) opp.grow( __t );
                return retval;
            });

            return cg; 
        };

        if ( (_head.size == t.size) ^ can_grow( {_head} )[0] )
            s = t.size + 1;

        if ( _head.size <= max( 0, t.size - 2) )
            s = 1;
        
        _head = _head.size >= (t.size + 1) ? tree(-1) : _head; 

        if ( _tail.size() && _tail[0].size == t.size )
            if ( can_grow( {_head, _tail[0]} )[1] )
                s += t.size + 1;

        if ( _tail.size() && _tail.back().size == t.size + 1 )
            if ( ! can_grow({_head, _tail.back()})[1] )
                s += t.size + 1;
        
        return s;
    }

    friend bool 
    operator<( const grow_st& g1, const grow_st& g2 )
    {
        int m1 = g1.makes(), m2 = g2.makes();
        int c1 = g1.t.grow_cost(g1.f), c2 = g2.t.grow_cost(g2.f);
        return m1 < m2 || ( m1 == m2 && c1 > c2 );
    }



public:
    tree t;
    forest f;
    vector<tree> almost_shadowed_by;
    vector<tree> almost_cast_shadow;
};



struct cut_st
{
    cut_st(const tree& _t, const forest& _f ):
        t(_t), f(_f), 
        almost_shadowed_by(t.is_almost_shadowed_by( _f.day + 1 )),
        cast_shadow(t.cast_shadow_to( _f.day + 1 ))
    {}
    cut_st( const cut_st& c ):
        t(c.t), f(c.f), 
        almost_shadowed_by(c.almost_shadowed_by),
        cast_shadow(c.cast_shadow)
    {}

    tree head() const 
    {
        auto f = max_element( almost_shadowed_by.begin(), almost_shadowed_by.end(), 
        [](const tree& t1, const tree& t2 ){ return t1.size < t2.size; });
        if ( f == almost_shadowed_by.end() ) 
            return tree(-1);
        else
            return *f;
    }


    int makes() const 
    {
        tree _head = head();
        vector<tree> tail = cast_shadow;
        forest& opp = *f.opponent;
        int s = 0; 
        if ( _head.size == 3 ) 
            s = 3;
        if ( _head.size == 2 )
            if ( ! ( _head.dormant || opp.waiting || _head.grow_cost(opp) >= opp.sun ) )
                s = 3;
        return 
            accumulate( tail.begin(), tail.end(), s, 
            [](const int _s, const tree& _t){ return _s - max( _t.size, 0 ); });
    }


    friend bool 
    operator<( const cut_st& g1, const cut_st& g2 )
    {
        return g1.makes() < g2.makes();
    }

public:
    tree t;
    forest f;
    vector<tree> almost_shadowed_by;
    vector<tree> cast_shadow;
};



#endif /* __DECISION__ */







int main()
{
    
    using board_types::coordinate;

    int sink; 
    cin >> sink; cin.ignore();
    for( int i=0; i < 37; ++i )  
        cin >> land::main_board[i];

    unique_ptr<board<tree>> p_main_tree_board = make_board<tree>();
    board<tree>& mt_board = *p_main_tree_board;
    cerr << "board set" << endl;

    // game loop
    while (1) {
        clock_t __start__ = clock();     
        //cerr << "tic per sec: " << CLOCKS_PER_SEC << endl;

    
        forest m_forest( &mt_board, true );
        cin >> m_forest;

        forest o_forest = m_forest;
        o_forest.mine = false;
        o_forest.opponent = &m_forest;
        m_forest.opponent = &o_forest;
        cin >> o_forest;

        int n; // the current amount of trees
        cin >> n; cin.ignore();
        for ( int _ = 0; _ < n; ++_  )
        {
            tree t;
            cin >> t; cin.ignore();
            t._board = &mt_board;
            mt_board[t.id] = t; 
        }
        cin >> n; cin.ignore(); // n possible actions
        for (int i = 0; i < n; i++) 
        {
            string l;
            getline(cin, l);
        }
        m_forest.set_size();
        o_forest.set_size();

        cerr << m_forest << o_forest;
        vector<tree> m_trees = m_forest.trees();
        vector<tree> o_trees = o_forest.trees();

        copy( m_trees.begin(), m_trees.end(), ostream_iterator<tree>(cerr));
        copy( o_trees.begin(), o_trees.end(), ostream_iterator<tree>(cerr));


        /*
        unique_ptr<board<tree>> p_seed_board = copy_board(mt_board); 
        board<tree>& s_board = *p_seed_board; 

        transform( s_board.cells.begin(), s_board.cells.end(), s_board.cells.begin(), 
        []( tree t ){ 
            if ( t.id > -1 && t.size < 2) t.size = 2;
            return t;
        });
        */

        bool end_turn = false;


        if ( m_forest.day > 9 && m_forest.size[3] > 0 && m_forest.sun >= 4 )
        {
            cerr << "eval cut" << endl;
            vector<tree> cutable;
            copy_if( m_trees.begin(), m_trees.end(), back_inserter(cutable), 
            [](const tree& t){ return !t.dormant && t.size == 3; });

            if (cutable.size() != 0)
            {
                cerr << "base" << endl;
                vector<cut_st> cuts;
                transform( cutable.begin(), cutable.end(), back_inserter(cuts),
                [&m_forest]( const tree& t ){ return cut_st(t, m_forest); });
    
                cerr << "sort it" << endl;
                sort(cuts.rbegin(), cuts.rend());
    
                cerr << "makes, cut" << endl;
                int m = cuts[0].makes();
                cerr << "threshold" << endl;
                if (  m > 0 || 
                    ( m_forest.nutrients < 16 && m > -2 ) ||
                    ( m_forest.day > 22 ) ) 
                {
                    cerr << "forest cut" << endl;
                    m_forest.cut(cuts[0].t);
                    cout << "COMPLETE " << cuts[0].t.id << endl;
                    end_turn = true;
                }
            }
        }


        

        vector<tree> upgradable;
        copy_if( m_trees.begin(), m_trees.end(), back_inserter(upgradable), 
        [&m_forest](const tree& t){ 
            return !t.dormant && t.size < 3 && m_forest.sun >= t.grow_cost(m_forest);
        });

        if ( !end_turn && upgradable.size() > 0 )
        {
            cerr << "eval grow" << endl;
            vector<grow_st> grows;
            transform( upgradable.begin(), upgradable.end(), back_inserter(grows), 
            [&m_forest](const tree& t) -> grow_st
            {
                return { t, m_forest };
            });

            sort(grows.rbegin(), grows.rend());

            cerr << "grow sorted #" << grows.size() << endl;
            for ( grow_st g :grows )
            {
                cerr << "m: " << g.makes() << " ";
            }
            cerr << endl;
            if ( grows[0].makes() >= 0 )
            {
                m_forest.grow(grows[0].t);    
                cout << "GROW " << grows[0].t.id << endl;
                end_turn = true;
            }
        }


        const int SEED_LIMIT = 40;

        if ( !end_turn && m_forest.size[2] + m_forest.size[3] > 1 && m_forest.size[0] == 0 )
        {
            cerr << "eval seed" << endl;
            unordered_map<int,seed_st> next_seed;
            for ( tree& t: m_trees) 
            {
                if ( t.dormant ) continue;

                if (next_seed.size() > SEED_LIMIT) break;

                cerr << "number of seed evaluated: " << next_seed.size() << endl;
                cerr << "seeds from: " << t;

                vector<land> cso = t.can_seed_on();

                cerr << "cso: #" << cso.size() << endl;

/*
                remove_if(cso.begin(), cso.end(), [&t,&m_trees](const land& l)
                {
                    coordinate cl = land::main_board.coord(l.id);
                    return 
                        none_of(m_trees.begin(), m_trees.end(), 
                        [&cl](const tree& t)
                        {
                            coordinate ct = t._board->coord(t.id);
                            return 
                                find_if( cl.begin(), cl.end(), 
                                [&ct,i=0](const int x) mutable { return x == ct[i++]; }); 
                        });
                });
                */
    
                for( const land& l : cso)
                {
                    auto f = next_seed.find(l.id);
                    if ( f != next_seed.end() )
                    {
                        cerr << "redundant origin" << endl;
                        f->second.origin.push_back(t);
                    }
                    else if (next_seed.size() <= SEED_LIMIT)
                    {
                        cerr << "new seed stat" << endl; 
                        next_seed.emplace(l.id, seed_st(l, t, m_forest));
                    }
                }
            }
    
            if ( next_seed.size() )
            {
                vector<seed_st> seeds;
                transform( next_seed.begin(), next_seed.end(), back_inserter(seeds), 
                [&next_seed]( const auto p ){ return p.second; });
        
                sort( seeds.rbegin(), seeds.rend() );
    
                auto _s_ = seeds.begin();
                auto ff = find_if( _s_->origin.begin(), _s_->origin.end(), 
                    [](const tree& t){ return !t.dormant; });
    
                if ( ff == _s_->origin.end() )
                    cerr << "cannot seed, all trees dormant" << endl;
                else
                {
                    m_forest.seed(*ff,_s_->l);
                    cout << "SEED " << ff->id << " " << _s_->l.id << endl;
                    end_turn = true;
                }
            }
            else 
                cerr << "cannot seed, no land free" << endl;
        }

        if ( !end_turn )
            cout << "WAIT" << endl;
    }
}
