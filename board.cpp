#include <iostream>
#include <algorithm>
#include <numeric>
#include <memory>
#include <iterator>
#include <variant>
#include <cmath>
#include <cassert>

#ifndef _BOARD_
#define _BOARD_

using namespace std;


/* board with 3 axes coordinates: ^x (north), <\y (west-north-west), </z (west-south-west)
*/                                           
namespace board_types 
{
  typedef int axis_value; 
  typedef int cell_id; 
  typedef int orientation; 
  typedef array<axis_value, 3>    coordinate;   // x,y,z with z = y-x
  typedef array<coordinate, 37>   coords_array; 
  typedef array<cell_id, 37>      index_array;
  typedef array<cell_id, 7>       offset_array;
};


ostream& operator<<( ostream& out, const board_types::coordinate& c )
{
  out << (c[0]>=0 ? " ":"") << c[0] << ", ";
  out << (c[1]>=0 ? " ":"") << c[1] << ", ";
  return out << (c[2]>=0 ? " ":"") << c[2];
}

constexpr auto _abs = []( const int i ){ return i < 0 ? -i : i; };

// compute the coordinates of every cell
constexpr auto board_coords = []()
{
  using board_types::coords_array;
  using board_types::coordinate;

  const auto _pos = []( const int a, const int b, const int c) 
  {
    return [=]( const int d )
      {
        return  ( a*_abs( (b-d+4)%3 - 1 ) - c*( (b-d+3)%3 - 1 )) * 
                ( 0 <= (b-d) && (b-d) < 3 ? 1 : -1);
      };
  };

  coords_array coords = {};
  coords[0] = {0,0,0};
  auto it = coords.begin() + 1; 
  for ( int a = 1; a < 4; ++a )
  {
    auto ite = it + a*6;
    for ( int k=0; it != ite; ++it, ++k )
    {
      int b = k / a, c = k % a;
      const auto pos = _pos(a,b,c); 
      *it = coordinate({ pos(0), pos(1), pos(2) });
    };
  }
  return coords;
};




// conversely sort indexes by coordinates 
constexpr auto board_index = []( const board_types::coords_array& coords )
{
  using board_types::coords_array;
  using board_types::index_array;
  using board_types::coordinate;
  using coord_id = array<int, 4>;

  index_array index = {0};
  array<coord_id, 37> sorted = {}; // x,y,z,id

  for( int i = 0; i < coords.size(); ++i )
  {
    const auto [x,y,z] = coords[i];
    sorted[i] = { x,y,z,i };
  } 
  const auto compare = []( coord_id& c1, coord_id& c2 )
  {
    return c1[0] < c2[0] || ( c1[0] == c2[0] && c1[1] < c2[1] );
  };
  for ( int i = 1; i < sorted.size(); ++i)
  {
    int j = i;
    while( j > 0 && compare(sorted[j], sorted[j-1]) )
    {
      auto tmp = move( sorted[j] ); 
      sorted[j] = move( sorted[j-1] );
      sorted[--j] = move( tmp );
    }
  }
  for ( int i = 0; i < sorted.size(); ++i)
    index[i] = sorted[i][3];

  return index;
};



// coordinate ranges do not have same number of elements  
// compute offset values for random coordinate access   
constexpr auto board_offset = []()
{
  using board_types::offset_array;

  offset_array offset = {};
  for ( int x = -3; x < 4; ++x )
    offset[x+3] = 18 + 6*x - x*(_abs(x)-1)/2;
  return offset;
};



template< typename T > 
struct board 
{
  using axis_value = board_types::axis_value;
  using cell_id = board_types::cell_id;
  using coords_array = board_types::coords_array; 
  using index_array = board_types::index_array; 
  using offset_array = board_types::offset_array;
  using coordinate = const board_types::coordinate;
  typedef T value_type;
  typedef T& reference;

  static constexpr coords_array coords = board_coords();
  static constexpr index_array  index  = board_index( coords ); 
  static constexpr offset_array offset = board_offset();

  constexpr board(): cells() {}
  constexpr board( const board<T>& b ): cells(b.cells) {}
  constexpr board( board<T>&& b ): cells( move(b.cells) ) {}

  reference operator[]( const cell_id i )
  {
    return cells[i];
  }

  coordinate& coord( const cell_id i ) const
  {
    return coords[i];
  }

  reference at( coordinate& c ) 
  {
    const auto [x,y,z] = c;
    return at(x,y,z);
  }

  reference at(const axis_value x, const axis_value y, const axis_value z) 
  {
    assert( z == y-x );
    return cells[ index[ offset[x+3] + y ] ];
  }

public:
  array<value_type, 37>   cells;
};




/* random access iterator. 
    e.g. range from one cell: 
        auto it0 = board_iterator(board, 0, cell_id);
        for ( auto it = it0 - 3; it != it0 + 3; ++it )
*/
template<typename T>
struct board_iterator 
{
  using value_type = T;
  using difference_type = std::ptrdiff_t;
  using pointer = T *;
  using reference = T&;
  using iterator_category = std::random_access_iterator_tag;

  using board_type = board<T>&;
  using coordinate = board_types::coordinate;
  using cell_id = board_types::cell_id;
  using orientation = board_types::orientation;


  constexpr board_iterator( board_type b, const cell_id i, const orientation o ) noexcept: 
    _board(b), ori(o), pos(_board.coord(i))
  {}
  constexpr board_iterator( board_type b, const coordinate& p, const orientation o ) noexcept: 
    _board(b), ori(o), pos(p)
  {}
  constexpr board_iterator( const board_iterator& it ) noexcept:
    _board(it._board), ori(it.ori), pos(it.pos)
  {}

  constexpr board_iterator end() const noexcept
  {
    auto [x,y,z] = pos;
    int u = 6, v = u, w = v;
    if (ori % 3 != 0 ) u =  ori      > 3 ? 3+x : 3-x;
    if (ori % 3 != 1 ) v = (ori+2)%6 > 3 ? 3-y : 3+y;
    if (ori % 3 != 2 ) w = (ori+1)%6 > 3 ? 3-z : 3+z;
    return *this + min(u, min(v,w)) + 1;
  }

  constexpr reference operator*() const noexcept 
  {
    return _board.at(pos);
  }
  constexpr pointer operator->() const noexcept 
  {
    return &_board.at(pos);
  }
  constexpr value_type operator[](const difference_type n) const noexcept
  {
    auto [x,y,z] = pos;
    if (ori % 3 != 0 ) x +=  ori      > 3 ? -n : n;
    if (ori % 3 != 1 ) y += (ori+2)%6 > 3 ?  n :-n;
    if (ori % 3 != 2 ) z += (ori+1)%6 > 3 ?  n :-n;
    return _board.at(x,y,z);
  }

  constexpr board_iterator & operator+=(const difference_type n) noexcept
  {
    auto& [x,y,z] = pos;
    if (ori % 3 != 0 ) x +=  ori      > 3 ? -n : n;
    if (ori % 3 != 1 ) y += (ori+2)%6 > 3 ?  n :-n;
    if (ori % 3 != 2 ) z += (ori+1)%6 > 3 ?  n :-n;
    return *this;
  }
  constexpr board_iterator & operator++() noexcept
  {
    return *this += 1;
  }
  constexpr board_iterator operator++(int)noexcept
  {
    board_iterator retval = *this;
    ++*this;
    return retval;
  }
  friend constexpr board_iterator 
  operator+(board_iterator lhs, const difference_type n) noexcept
  {
    return lhs += n;
  }

  constexpr board_iterator & operator-=(const difference_type n) noexcept
  {
    return *this += -n;
  }
  constexpr board_iterator & operator--() noexcept
  {
    return *this -= 1;
  }
  constexpr board_iterator operator--(int)noexcept
  {
    board_iterator retval = *this; 
    --*this;
    return retval;
  }
  friend constexpr board_iterator 
  operator-(board_iterator lhs, const difference_type n) noexcept
  {
    return lhs -= n;
  }

  friend constexpr difference_type 
  operator-(const board_iterator& lhs, const board_iterator& rhs) 
  {
    assert( ( lhs.ori - rhs.ori ) % 6 == 0 );  
    const auto [lx,ly,lz] = lhs.pos;
    const auto [rx,ry,rz] = rhs.pos;
    assert( lhs.ori % 3 != 0 || lx == rx );  
    assert( lhs.ori % 3 != 1 || ly == ry );  
    assert( lhs.ori % 3 != 2 || lz == rz );  
    return  lhs.ori % 3 != 0 ? 
                 lhs.ori < 3 ? lx - rx : rx - lx : 
                 lhs.ori > 0 ? ly - ry : ry - ly;
  } 

  friend constexpr bool operator==(
      const board_iterator& lhs, const board_iterator& rhs) noexcept
  {
      return ( lhs.ori - rhs.ori )%6 == 0 && lhs.pos == rhs.pos;
  }
  friend constexpr bool operator!=(
      const board_iterator& lhs, const board_iterator& rhs) noexcept
  {
      return !(lhs == rhs);
  }
 
public:
  board_type _board;
  orientation ori;
  coordinate pos; 

};


/* circular board iterator. 
    e.g. arc of radius 2 centered on one cell: 
        auto it0 = arc_iterator(board, cell_id, 2);
        for ( auto it = it0; it != it0.end(); ++it )
*/
template<typename T>
struct arc_iterator : public board_iterator<T>
{
  using value_type = T;
  using difference_type = std::ptrdiff_t;
  using pointer = T *;
  using reference = T&;
  using iterator_category = std::bidirectional_iterator_tag;

  using coordinate = board_types::coordinate;
  using cell_id = board_types::cell_id;
  using orientation = board_types::orientation;
  using board_type = board<T>&;

  using array_limits = array<difference_type,7>;


  constexpr array_limits find_limits() const noexcept 
  {
    array_limits l;
    board_iterator it(this->_board, center, 0); 
    generate( l.begin(), l.end() - 1, 
    [&,o=0]() mutable
    { 
      it.ori = o++;
      return min( it.end() - it - 1, radius );
    });
    l[6] = l[0];
    return l;
  }

  template< bool clockwise = false, 
            typename It1 = array_limits::iterator,
            typename It2 = array_limits::reverse_iterator,
            typename Cond = conditional_t< clockwise, It1, It2 > >
  constexpr void init() noexcept
  {
    array_limits limits = find_limits();

    variant<It1,It2> vlstart, vlend;
    if ( clockwise )
       vlstart = limits.begin(),  vlend = limits.end();
    else 
       vlstart = limits.rbegin(), vlend = limits.rend();
    Cond lstart = get<Cond>(vlstart), lend = get<Cond>(vlend);

    Cond _lstart = find_if( lstart, lend, [&](const int li){ return li >= radius; } );

    Cond f = find_if( _lstart + ( _lstart == lstart ? 1 : 0 ), lend, 
    [&]( const difference_type i)  
    {
      return i < radius;
    });
    --f;

    int a0 = 0, a1 = 0, a2 = 0, &ori = this->ori;
    if ( clockwise )
       a0 = f - lstart,     a1 = (6 + a0 - 1)%6, a2 = (a0 + 1)%6; 
    else
       a0 = lstart - f + 6, a1 = (a0 + 1)%6, a2 = (6 + a0 - 1)%6; 
    
    cerr << "a0: " << a0 << " a1: " << a1 << " a2: " << a2 << endl;

    ori = a0;
    this->pos = center;
    this->board_iterator<T>::operator+=(radius);

    ori = a1;
    tan = radius;
    if ( ++f != lend ) tan -= limits[a2];
    this->board_iterator<T>::operator-=(radius - tan);
  }

public:
  constexpr arc_iterator( board_type b, const coordinate& c, const difference_type r ) noexcept:
    board_iterator<T>(b, 0, c), center(c), radius(r), _start(true)
  {
    init();
  }
  constexpr arc_iterator( board_type b, const cell_id i, const difference_type r ) noexcept:
    board_iterator<T>(b, 0, i), center(b.coord(i)), radius(r), _start(true)
  {
    init();
  }
  constexpr arc_iterator( const arc_iterator& oth ) noexcept:
    board_iterator<T>(oth), center(oth.center), radius(oth.radius)
  {}
  constexpr arc_iterator end() noexcept
  {
    arc_iterator it = *this;
    it.init<true>();
    if ( it.pos == this->pos )
    {
      it.ori += 2;
      it.ori %= 6;
      it._start = false;
    }
    else
    {
      it.ori += 3;
      it.ori %= 6;
      it.tan = radius - it.tan;
      ++it;
    }
    return it;
  }

  constexpr value_type operator[](const difference_type n) const = delete;
  constexpr arc_iterator & operator+=( const difference_type n ) = delete;
  constexpr arc_iterator & operator++() noexcept
  {
    if ( tan == radius ) 
    { 
      this->ori += 1;
      this->ori %= 6;
      tan = 0;
    }
    ++tan;
    _start = false;
    this->board_iterator<T>::operator++();
    return *this;
  }
  constexpr arc_iterator operator++(int) noexcept
  {
    arc_iterator it = *this;
    ++*this;
    return it;
  }

  constexpr arc_iterator & operator-=( const difference_type n ) = delete;
  constexpr arc_iterator & operator--() noexcept
  {
    if ( tan == 0 ) 
    { 
      this->ori += 5;
      this->ori %= 6;
      tan = radius;
    }
    --tan;
    _start = true;
    this->board_iterator<T>::operator--();
    return *this;
  }
  constexpr arc_iterator operator--(int) noexcept
  {
    arc_iterator it = *this;
    --*this;
    return it;
  }

  friend constexpr arc_iterator 
  operator+(arc_iterator lhs, const difference_type n) = delete;
  friend constexpr arc_iterator 
  operator-(arc_iterator lhs, const difference_type n) = delete;
  friend constexpr difference_type 
  operator-(const arc_iterator& lhs, const arc_iterator& rhs) = delete; 
  friend constexpr bool operator==(
      const arc_iterator& lhs, const arc_iterator& rhs) noexcept
  {
      return lhs.pos    == rhs.pos && 
             lhs.center == rhs.center && 
             lhs.ori    == rhs.ori &&
             lhs._start == rhs._start; // differentiate end() from begin() in circle;
  }
  friend constexpr bool operator!=(
      const arc_iterator& lhs, const arc_iterator& rhs) noexcept
  {
      return !( lhs == rhs );
  }

public:
  const coordinate center;
  const difference_type radius;
  difference_type tan;
  bool _start;
};




#endif /* __BOARD__ */



