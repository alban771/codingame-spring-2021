#include <iostream>
#include <iterator>
#include "board.cpp"

using namespace std;
using board_types::coordinate;


/*
ostream & operator<<(ostream& out, const coordinate& c )
{
  auto [x,y,z] = c;
  const auto print = [&out]( const char s, const int p )
  { out << s << ( p < 0 ? ":" : ": " ) << p << ",  "; };
  print('x',x);
  print('y',y);
  print('z',z);
  return out;
}
*/


void try_board_index( board<int> board ) 
{
  cerr << "board cells: \n\t";
  copy( board.cells.begin(), board.cells.begin()+10, ostream_iterator<int>(cerr, ", ") );
  cerr << endl;

  for ( int i = 0; i < 10; ++i )
    cerr << "coordinate #" << i << ": " << board.coord(i) << endl;

  cerr << "index 0-10: \n\t";
  copy( ::board<int>::index.begin(), ::board<int>::index.begin() + 10, ostream_iterator<int>(cerr, ", ") );
  cerr << ::board<int>::index[10] << endl;

  cerr << "index 10-20: \n\t";
  copy( ::board<int>::index.begin()+10, ::board<int>::index.begin() + 20, ostream_iterator<int>(cerr, ", ") );
  cerr << ::board<int>::index[20] << endl;

  cerr << "index 20-30: \n\t";
  copy( ::board<int>::index.begin()+20, ::board<int>::index.begin() + 30, ostream_iterator<int>(cerr, ", ") );
  cerr << ::board<int>::index[30] << endl;


  cerr << "offset: \n\t";
  copy( ::board<int>::offset.begin(), ::board<int>::offset.end() - 1, ostream_iterator<int>(cerr, ", ") );
  cerr << ::board<int>::offset.back() << endl;

  cerr << "index [0,0,0]:" << board.at( 0, 0, 0 ) << endl;
  cerr << "index [0,-1,-1]:" << board.at( 0, -1, -1 ) << endl;
  cerr << "index [1,0,-1]:" << board.at( 1, 0, -1 ) << endl;
  cerr << "index [1,1,0]:" << board.at( 1, 1, 0 ) << endl;
  cerr << "index [0,1,1]:" << board.at( 0, 1, 1 ) << endl;
}


void try_board_iterator( board<int> board )
{
  using it_t = board_iterator<int>;
  for ( int o = 0; o < 6; ++o )
  {
    cerr << "\ndirection: " << o << endl;
    board_iterator it( board, 0, o );
    cerr << "iter +3 from center: " << board.coord(*(it + 3)) << endl;
    cerr << "iter -1 from center: " << board.coord(*(--it)) << endl;
    board_iterator ite = it.end();
    cerr << "iteration end: " << board.coord(*(ite - 1)) << endl;
    cerr << "difference: " << ite - it << endl;  
  }

  /*
  try {
    cerr << "difference #8 - #1, dir 0: " << it_t(board, 8, 0) - it_t(board, 1, 0) << endl;  
  } catch (...) {
    cerr << " assert exception" << endl; 
  }
  */
  cerr << "difference #8 - #3, dir 0: " << it_t(board, 8, 0) - it_t(board, 3, 0) << endl;  
  cerr << "difference #8 - #3, dir 3: " << it_t(board, 8, 3) - it_t(board, 3, 3) << endl;  

  for (board_iterator it = {board, {1,-1,-2}, 4};it != it.end(); ++it )
    cerr << "iterator pos: " << board.coord( *it ) << endl;

}


void try_arc_iterator( board<int> board )
{
  using arc_it = arc_iterator<int>;

  
  /*
  
  arc_it it( board, 0, 1 ), ite = it.end();
  cerr << "begin #0, r1, ori: " << it.ori  << " _start:" << it._start  << " center:" << it.center  << endl;
  cerr << "end   #0, r1, ori: " << ite.ori << " _start:" << ite._start << " center:" << ite.center << endl;

  for ( ; it != ite; )
    cerr << "arc pos: " << board.coord( *++it ) << endl;


  cerr << "init  #0, r2: " << arc_it(board, 0, 2).pos << endl;
  cerr << "end   #0, r2: " << arc_it(board, 0, 2).end().pos << endl;
  cerr << "init #28, r1: " << arc_it(board, 28, 1).pos << endl;
  cerr << "end  #28, r1: " << arc_it(board, 28, 1).end().pos << endl;
  cerr << "init #13, r2: " << arc_it(board, 13, 2).pos << endl;
  cerr << "end  #13, r2: " << arc_it(board, 13, 2).end().pos << endl;
  cerr << "init #28, r2: " << arc_it(board, 28, 2).pos << endl;
  cerr << "end  #28, r2: " << arc_it(board, 28, 2).end().pos << endl;

  arc_it it2 = arc_it(board, 28, 2), ite2 = it2.end();
  for ( ; it2 != ite2; )
    cerr << "arc pos: " << it2++.pos  << endl;

    */
  /*
  cerr << "arc   #28, r2: " << it2.pos  << endl; 
  cerr << "end   #28, r2: " << ite2.pos << endl; 
  cerr << "arc   #28, r2, ori: " << it2.ori  << " _start:" << it2._start  << " center:" << it2.center  << endl;
  cerr << "end   #28, r2, ori: " << ite2.ori << " _start:" << ite2._start << " center:" << ite2.center << endl;
  */


  auto it11 = arc_it(board, 16, 3), it11e = it11.end();
  cerr << "init #16, r3: " << it11.pos << endl;
  for ( int _=0; _<6; ++_ )
    cerr << "arc pos: " << (++it11).pos  << endl;

  cerr << "past the end: " << it11e.pos << endl;
  cerr << "the end: " << (--it11e).pos << endl;

  /*
  auto _it11 = arc_it(board, 16, 6);
  _it11.init<true>();
  cerr << "init clokwise" << _it11.pos << endl;
  cerr << "end  #11, r2: " << (--arc_it(board, 11, 2).end()).pos << endl;
  */
}


int main()
{
  board<int> board;
  iota( board.cells.begin(), board.cells.end(), 0 );

  //try_board_index( board );
  //try_board_iterator( board );
  try_arc_iterator( board );

}
