#include <vector>
#include <algorithm>

using namespace std



#ifndef __DECISION__
#define __DECISION__

struct seed_st
{
    seed_st(const land& _l, const tree& t, const forest& _f ):
        l(_l), origin(1,t), f(_f), 
        friendly_shadow(l.friendly_shadow(_f)),
        sunny_days(l.sunny_days(_f)),
    {}

    const land& l; 
    const forest& f;
    vector<tree> origin;
    vector<tree> friendly_shadow;
    const vector<int> sunny_days;

    vector<int> shift_sunny( int offst ) const
    {        
        vector<int> sd(sunny_days.size());
        transform( sunny_days.begin(), sunny_days.end(), sd.begin(), 
        [&offst](const int i){ return (6 + i - offst%6)%6; });
        sort( sd.begin(), sunny_days.end() );
    }

    friend bool
    operator<( const seed_st& l1, const seed_st& l2 );
    {
        if ( l1.friendly_shadow.size() == l2.friendly_shadow.size() )
        {
            vector<int> week (6), sd1 = l1.shift_sunny(f.day+2), sd2 = l1.shift_sunny(f.day+2);
            iota( week.begin(), week.end(), 0);
            
            auto [ _,f1] = mismatch( sd1.begin(), sd1.end(), week.begin() );
            auto [__,f2] = mismatch( sd2.begin(), sd2.end(), week.begin() );
            
            if ( f1 == f2 )
                return l1.richness < l2.richness;
            else
                return f1 < f2;
        }
        else
            return l1.friendly_shadow.size() < l2.friendly_shadow.size();
    }
};


struct grow_st
{
    grow_st(const tree& _t, const forest& _f ):
        t(_t), f(_f), 
        almost_shadowed_by(t.is_almost_shadowed_by( _f.day + 1 )),
        almost_cast_shadow(t.almost_cast_shadow_to( _f.day + 1 )),
    {}

    vector<tree> tail() const
    {
        vector<tree> tail;
        copy_if( almost_cast_shadow.begin(), almost_cast_shadow.end(), back_inserter(tail),
        [&]( const tree& _t )
        {
            return _t.size == t.size || _t.size == t.size + 1;
        });
        return of;
    }
    tree head() const 
    {
        auto f = max_element( almost_shadowed_by.begin(), almost_shadowed_by.end(), 
        [](const tree& t1, const tree& t2 ){ return t1.size < t2.size; });
        if ( f == almost_shadowed_by.end() ) 
            return tree(-1);
        else
            return *f;
    }

    int makes() const
    {
        int s = 0;
        const tree& _head = head();
        const vector<tree> _tail = tail();

        //TODO: impl on forest
        const auto can_grow = 
        [opp=*f.opponent]( const vector<tree>& opp_t ) -> vector<bool>
        {
            vector<bool> cg(opp_t.size());

            transform( opp_t.begin(), opp_t.end(), cg, 
            [opp,r=0]( tree __t) mutable
            {
                if (__t.size == -1 ) return false;
                opp.sun += r == __t.size;
                r = __t.size+1;
                bool retval = ! ( __t.dormant || opp.waiting || __t.grow_cost(opp) >= opp.sun );
                opp.grow( __t );
                return retval;
            });

            return cg; 
        };

        if ( (_head.size == t.size) ^ can_grow( {_head} )[0] )
            s = t.size + 1;

        if ( _head.size < t.size - 1 )
            s = 1;
        
        if ( _tail.size() && _tail[0].size == t.size )
            if ( can_grow( {_head, _tail[0]} )[1] )
                s += t.size + 1;

        if ( _tail.size() && _tail.back().size == t.size + 1 )
            if ( ! can_grow({_head, _tail.back()})[1] )
                s += t.size + 1;
        
        return s;
    }

    friend bool 
    operator<( const grow_st& g1, const grow_st& g2 )
    {
        return g1.makes() < g2.makes();
    }



public:
    const tree& t;
    const forest& f;
    vector<tree> almost_shadowed_by;
    vector<tree> almost_cast_shadow;
};



struct cut_st
{
    cut_st(const tree& _t, const forest& _f ):
        t(_t), f(_f), 
        almost_shadowed_by(t.is_almost_shadowed_by( _f.day + 1 )),
        cast_shadow(t.cast_shadow_to( _f.day + 1 ))
    {}


    tree head() const 
    {
        auto f = max_element( almost_shadowed_by.begin(), almost_shadowed_by.end(), 
        [](const tree& t1, const tree& t2 ){ return t1.size < t2.size; });
        if ( f == almost_shadowed_by.end() ) 
            return tree(-1);
        else
            return *f;
    }


    int makes() const 
    {
        tree _head = head();
        vector<tree> tail = cast_shadow;
        forest& opp = *f.opponent;
        int s = 0; 
        if ( _head.size == 3 ) 
            s = 3;
        if ( _head.size == 2 )
            if ( ! ( _head.dormant || opp.waiting || _head.grow_cost(opp) >= opp.sun ) )
                s = 3;
        return 
            accumulate( tail.begin(), tail.end(), s, 
            [](const int _s, const tree& _t){ return _s - max( _t.size, 0 ); });
    }


    friend bool 
    operator<( const cut_st& g1, const cut_st& g2 )
    {
        return g1.makes() < g2.makes();
    }

public:
    const tree& t;
    const forest& f;
    vector<tree> almost_shadowed_by;
    vector<tree> cast_shadow;
};



#endif /* __DECISION__ */


/*
struct grow_st
{
    vector<tree* const> foes;
}




for ( tree& t: m_trees)
{
    vec
    t.range({ _day% })
}








unordered_map<int,seed_stat> next_seed;
for ( tree& t: m_trees) 
{
    vector<land> cso = land::main_board[t.id].arc(2);
    remove_if(cso.begin(), cso.end(), [&t,&m_trees](const land& l)
            {
            coordinate cl = land::main_board.coord(l.id);
            return 
            none_of(m_trees.begin(), m_trees.end(), 
                    [&cl](const tree& t)
                    {
                    coordinate ct = t._board->coord(t.id);
                    return 
                    find_if( cl.begin(), cl.end(), 
                            [&ct,i=0](const int x) mutable { return x == ct[i++]; }); 
                    });
            });

    for( const land& l : cso)
    {
        if ( next_seed.find(l.id) != next_seed.end() )
            next_seed[l.id].origin.push_back(&t);
        else
            next_seed.emplace(l.id, seed_stat{l, t});
    }
}






typedef pair<tree, int> tree_balance;
typedef pair<land, int> seed_balance;

constexpr auto grow_value( const int day ) 
{
    return [&day](const tree& t) -> tree_balance 
        { 
            int o = (day + 1) % 6;
            const array<int,3> a = shadowing(t,o);
            const auto [h,s] = spooky(t,o);
            int i = t.size;
            return {t, ( i > 0 ? a[i] - a[i-1] : a[i] ) +
                        ( h ? i == s ? i + 1 : 0 : 1 ) +
                        i}; 
        };
}


constexpr auto cut_value( const int day ) 
{
    return [&day](const tree& t) -> tree_balance 
        { 
            int o = (day + 1) % 6;
            const array<int,3> a = shadowing(t,o);
            const auto [h,s] = spooky(t,o);
            int i = t.size;
            return {t, - a[i] - ( h ? 0 : i ) + i}; 
        };
}



constexpr auto seed_value( const int day )
{
    using tree_dist=tree::tree_dist;
    return [&day](const int cell_id ) -> seed_balance
        {
            const land l = board::get(cell_id);
            if ( !l.richness || l._tree.get() )
                return { l, -1 };

            tree t(cell_id);
            const vector<tree_dist> r = t.range();
            bool sh_mine =  any_of(r.begin(), r.end(), 
                            [](const tree_dist& td){ return td.first.mine; });
            bool sunny = !spooky( t, day+1 ).first;
            const auto [x,y,z] = board::get().index[cell_id];

            if ( l.richness == 1 )
            {
                return { l, sh_mine ? -1 : sunny + (x==3 || y==3 || z==3) };
            }

            if ( l.richness == 2 )
            {
                return { l, sh_mine ? -1 : sunny };
            }

            return { l, sh_mine ? -1 : sunny + 1 };
        };
}

const auto count_shadow = [&v]( const int _size ) { 
        return 
            accumulate( v.begin(), v.end(), 0, 
            [&](const int s, const auto& p )
            { 
                const tree& _t = p.first;
                return s + _t.size > _size ? 0 : _t.size * (_t.mine ? -1 : 1); 
            });
    };
    return { t.size > 1 ? 0 : count_shadow(1),  
             t.size > 2 ? 0 : count_shadow(2),  
                              count_shadow(3) };

/*  for one tree, for one day, 
    check if the tree is shadowed and the max size of every shadower
 */
//pair<bool,int> spooky( const tree& t, const int ori )
//{
//    using tree_dist=tree::tree_dist;
//
//    vector<tree_dist> v = t.range( (ori + 3) % 6 );
//    vector<int> v_size( v.size() );
//    transform( v.begin(), v.end(), v_size.begin(),
//    [&t](const tree_dist& td)
//    {
//        const auto& [_t,d] = td;
//        return _t.size >= t.size && _t.size >= d ? _t.size : 0; 
//    });
//    const auto m = max_element( v_size.begin(), v_size.end() );
//    return { v.size() && *m > 0, v.size() ? *m : 0 };
//}
//
//// return optimized cost from lhs to rhs  
//    friend int 
//    cost_to( const forest& lhs, const forest& rhs );


/*  for two forest sizes
    compute the sum of sun points needed to upgrade from first to second
*/
//TODO: test me
//int operator->( const forest& cur, const forest& tgt ) 
//{
//    using forest::size_f;
//    static const auto upgrade_cost = [&cur](const int size, const int up, const int next_up)
//    {
//        int s = up * tree::base_cost[size];
//        if ( size == 4 )
//            return s;
//        return s + up*( cur.size[size] - next_up + (up-1)/2 );
//    };
//    array<int,5> upgd;
//    size_f diff = tgt.size - cur.size;
//    upgd[4] = max(0,-accumulate(diff.begin(), diff.end(),0)); 
//    transform( diff.rbegin(), diff.rend(), upgd.rbegin(), upgd.rbegin()+1,
//    [](const int d, const int u){ return d+u; });
//    return 
//        accumulate( upgd.rbegin(), upgd.rend(), 0, 
//        [next_up=0,size=4](const int s, const int up) mutable 
//        { 
//            int _s = s + upgrade_cost(size--, up, next_up); 
//            next_up = up;
//            return _s;
//        });  
//}



/*
 *
 * From web ide
 *
 */
//        vector<tree> m_forest;
//        copy_if( forest.begin(), forest.end(), back_inserter(m_forest),
//        [](const tree& t){ return t.mine; });
// 
//        forest_size m_size = size( m_forest );
//
//        cerr << "m_size: " << m_size << endl;
//
        /* find every possible growth
        int max_seed = accumulate(m_size.begin(), m_size.end(), 0);
        
        set<forest_size> s_upgds;
        {
            vector<forest_size> upgds; 
            for ( int i = 4; i >= 0; ++i )
            {
                upgds.push_back( m_size ); 
                for ( forest_size up : upgds )
                {
                    forest_size pm_size, pup;
                    partial_sum(m_size.begin(), m_size.end(), pm_size.begin());
                    while( grow_cost(m_size, up) <= sun )  
                    {
                        partial_sum( up.begin()+1, up.end(), pup.begin()+1 );
                        if ( any_of( pup.begin()+1, pup.end(), [&pm_size,i=0](const int p) mutable {
                            return p > pm_size[i++];
                        })) break;
                        ++up[i];
                    }
                    --up[i];
                }
            }
            for ( forest_size up : upgds )
                s_upgds.insert(up);
        }
        */

        

        
        /*
        const auto compare_balance = [](const tree_balance& tb1, const tree_balance& tb2)
        {
            return tb1.second > tb2.second;
        };


        vector<tree_balance> balance_cut(m_forest.size());
        transform( m_forest.begin(), m_forest.end(), balance_grow.begin(), cut_value(day) );
        balance_cut.erase( 
            remove_if(balance_cut.begin(), balance_cut.end(), 
            []( const tree_balance& tb)
            { 
                const auto& [t,_] = tb;
                return t.size < 3;
            }), 
            balance_cut.end());
        sort( balance_cut.begin(), balance_cut.end(), compare_balance );
        
        cerr << "cut balance: " << balance_cut[0].second << endl;


        vector<tree_balance> balance_grow(m_forest.size());
        transform( m_forest.begin(), m_forest.end(), balance_grow.begin(), grow_value(day) );
        sort( balance_grow.begin(), balance_grow.end(), compare_balance );


        cerr << "grow balance: " << balance_grow[0].second << endl;
        


        if ( sun > 4 && balance_cut.size() && 
                  balance_cut[0].second > 0 ) 
            cout << "COMPLETE " << balance_cut[0].first.cell_id << endl;
            continue;


        auto f = find_if( balance_grow.begin(), balance_grow.end(), 
        [&]( const tree_balance& tb ){
            return  sun >= single_grow_cost(m_size, tb.first ) &&
                    tb.second > 0;
        });
        if ( f != balance_grow.end() )
            cout << "GROW " << f->first.cell_id << endl;
            continue;
        



        int income = income(m_forest, (day+1) % 6 );



        else if ( m_size[0] == 0 && sun + income(m_forest, (day+1)%6 ) )
        {
            int from = balance_grow[0].first.cell_id;
            board_iterator<land> it({0,0,0}, 0);
            auto l = find_if( it-1, it+2, []( const land& l){ return l.richness > 0; } );
            if ( l == it+2 ) 
                throw misplay("Cannot find passably rich soil");
            int to = l->id; 
            cout << "SEED " << from << " " << to << endl;
        }

        else 
            cout << "WAIT" << endl;

            */


        /*
        vector<vector<tree_balance>> balance_by_size(3);

        generate( balance_by_size.begin(), balance_by_size.end(), [i=0,&m_forest,&day]() mutable
        {
            vector<tree, int> b(m_forest.size());
            transform( m_forest.begin(), m_forest.end(), b.begin(),
            [i,&day](const tree& t) -> tree_balance 
            { 
                int o = (day + 1) % 6;
                const array<int,3> a = shadowing(t,o);
                const auto [h,s] = spooky(t,o);
                return {t, ( i > 0 ? a[i] - a[i-1] : a[i] ) +
                           ( h ? 1 : i == s ? i + 1 : 0 ) +
                            i}; 
            });
            b.erase( 
                remove_if( b.begin(), b.end(), 
                [i]( const tree& t, const int _b )
                { 
                    return t.size > i; 
                }), b.end() );
            sort( b.begin(), b.end(), [](const tree_balance& tb1, const tree_balance& tb2)
            {
                return tb1.second > tb2.second;
            });
            ++i;
            return b;
        });
        */


/*

        using plan_score=pair<vector<tree>, int>;
        vector<plan_score> upgd_trees;
        transform( s_upgds.begin(), s_upgds.end(), upgd_trees.begin(),
        [&](const forest_size & up ) -> plan_score
        {
            vector<tree> h;
            int s;

            for ( int i = 1; i < 4; ++i )
            {
                vector<tree_balance> b = balance_by_size[i-1];
                b.erase( 
                    remove_if(b.begin(), b.end(), [&i,&h](const tree& t, const int _)
                    {
                        if ( i == t.size + 1 ) return false;  

                        int c = count( h.begin(), h.end(), t ); 
                        return i != t.size + c + 1;
                    }), b.end());
                
                assert( up[i] < b.size() );

                copy_n( b.begin(), b.size()-up[i], back_inserter(h) );
                s += accumulate( b.begin(), b.end()-up[i], 0, 
                [](const int w, const auto & tb){ return w + tb.second; });
            }

            return {h,s};
        });
        
        */

        



        

        /*
        sort(trees.begin(), trees.end(), 
        [](const tree& t1, const tree& t2){
            return t1.cell.rich > t2.cell.rich;
        });
        */
       



        //cerr << "sun: " << sun << endl;
        //cerr << "grow cost from: " << t_sizes << " to: " << target << " = " << grow_cost(t_sizes, target) << endl;

        // GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>

        /*
        auto f = find_if( balance_by_size.rbegin(), balance_by_size.rend(), 
        [&]( const auto& b ){
            return  b.size() && 
                    sun >= single_grow_cost(m_size, b[0].first ) && 
                    b[0].second > 0; 
        });
        if ( f != balance_by_size.rend() )
            cout << "GROW " << (*f)[0].first.cell_id << endl;
        */

            */

